package com.simrussia.buildsrc

import com.simrussia.buildsrc.Deps
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project

class AndroidConfigPlugin : Plugin<Project> {

    internal companion object {
        const val COMPILE_SDK_VERSION = 31
        const val TARGET_SDK_VERSION = 31
        const val MIN_SDK_VERSION = 23
        const val BUILD_TOOLS_VERSION = "30.0.3"
    }

    override fun apply(project: Project) {
       // project.configureAndroidProject()
        project.configureDefaultDependencies()
    }

    private fun Project.configureDefaultDependencies() = with(dependencies) {

    }


}