package com.simrussia.buildsrc

object Deps {
    const val androidCore = "androidx.core:core-ktx:1.7.0"
    const val androidCompat = "androidx.appcompat:appcompat:1.4.1"
    const val googleMaterial = "com.google.android.material:material:1.5.0"
    const val constraintlayout = "androidx.constraintlayout:constraintlayout:2.1.3"
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:2.4.1"
    const val navigationUI = "androidx.navigation:navigation-ui-ktx:2.4.1"
    const val fragment = "androidx.fragment:fragment-ktx:1.4.1"
    const val lottie = "com.airbnb.android:lottie:4.2.2"
    const val hilt = "com.google.dagger:hilt-android:2.40.5"
    const val hiltCompiler = "com.google.dagger:hilt-compiler:2.40.5"
    const val recyclerviewAnimators = "jp.wasabeef:recyclerview-animators:4.0.2"
    const val kirich1409Viewbindingpropertydelegate =
        "com.github.kirich1409:viewbindingpropertydelegate:1.5.3"
    const val xwrayGroupie = "com.xwray:groupie:2.9.0"
    const val groupieViewbinding = "com.xwray:groupie-viewbinding:2.9.0"
    const val mask = "com.redmadrobot:input-mask-android:6.0.0"
    const val retrofit = "com.squareup.retrofit2:retrofit:2.9.0"
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.2"
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.5.2"
    const val gson = "com.squareup.retrofit2:converter-gson:2.9.0"
    const val http = "com.squareup.okhttp3:okhttp:4.8.0"
    const val interceptor = "com.squareup.okhttp3:logging-interceptor:4.8.0"
    const val viewbindingpropertydelegateNoreflection =
        "com.github.kirich1409:viewbindingpropertydelegate-noreflection:1.5.3"
    const val junit = "junit:junit:4.+"
    const val junitAndroid = "androidx.test.ext:junit:1.1.3"
    const val espresso = "androidx.test.espresso:espresso-core:3.4.0"
}