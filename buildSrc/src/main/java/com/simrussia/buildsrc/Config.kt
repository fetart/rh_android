package com.simrussia.buildsrc

object Config {
    const val COMPILE_SDK_VERSION = 31
    const val TARGET_SDK_VERSION = 31
    const val MIN_SDK_VERSION = 23
    const val BUILD_TOOLS_VERSION = "30.0.3"
}