package com.simrussia.mappers.base

interface Mapper<Source, Target> {
    fun map(src: Source): Target
}

interface ReverseMapper<Source, Target> : Mapper<Source, Target> {
    fun reverseMap(target: Target): Source
}