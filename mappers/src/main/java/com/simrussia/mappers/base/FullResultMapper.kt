package com.simrussia.mappers.base

import com.simrussa.remote.BaseResponse
import com.simrussia.result.Result
import com.simrussia.result.asFailure
import com.simrussia.result.asSuccess
import com.simrussia.result.isSuccess

abstract class FullResultMapper<Source, Target> :
    Mapper<Result<BaseResponse<Source>>, Result<Target>> {

    protected abstract fun mapSuccessResult(src: BaseResponse<Source>): Target

    override fun map(src: Result<BaseResponse<Source>>): Result<Target> {
        return if (src.isSuccess()) {
            src.asSuccess().value.success?.let {
                return if (it)
                    Result.Success.Value(mapSuccessResult(src.asSuccess().value))
                else Result.Failure.ErrorMessages(Throwable(src.asSuccess().value.message.toString()))
            } ?: Result.Success.Value(mapSuccessResult(src.asSuccess().value))
        } else {
            Result.Failure.ErrorMessages(src.asFailure().error)
        }
    }
}