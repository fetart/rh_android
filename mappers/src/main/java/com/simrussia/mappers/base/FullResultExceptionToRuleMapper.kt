package com.simrussia.mappers.base

import com.simrussa.remote.BaseResponse
import com.simrussia.result.Result
import com.simrussia.result.asFailure
import com.simrussia.result.asSuccess
import com.simrussia.result.isSuccess

abstract class FullResultExceptionToRuleMapper<Source, Target> :
    Mapper<Result<BaseResponse<Source>>, Result<Target>> {

    protected abstract fun mapSuccessResult(src: BaseResponse<Source>): Target

    override fun map(src: Result<BaseResponse<Source>>): Result<Target> {
        return if (src.isSuccess()) {
            src.asSuccess().value.success?.let {
                Result.Success.Value(mapSuccessResult(src.asSuccess().value))
            } ?: Result.Success.Value(mapSuccessResult(src.asSuccess().value))
        } else {
            Result.Failure.ErrorMessages(src.asFailure().error)
        }
    }
}