package com.simrussia.mappers.user

import com.simrussa.models.user.BonusDom
import com.simrussa.models.user.UserDom
import com.simrussa.remote.models.user.UserResponse
import com.simrussia.mappers.base.ResultMapper
import javax.inject.Inject

class UserMapper @Inject constructor(
) : ResultMapper<UserResponse, UserDom>() {
    override fun mapSuccessResult(src: UserResponse?) = UserDom(
        id = src?.id ?: "",
        name = src?.name ?: "",
        surname = src?.surname ?: "",
        email = src?.email ?: "",
        deviceId = src?.deviceId ?: "",
        profCustomer = src?.profCustomer ?: false,
        bonus = BonusDom(src?.bonus?.prize ?: "", src?.bonus?.level ?: ""),
    )
}

