package com.simrussia.mappers.home

import com.simrussa.models.home.CategoryDom
import com.simrussa.remote.models.home.HomeResponse
import com.simrussia.mappers.base.ResultMapper
import javax.inject.Inject

class CatalogMapper @Inject constructor(
) : ResultMapper<Map<String, HomeResponse>, List<CategoryDom>>() {
    override fun mapSuccessResult(src: Map<String, HomeResponse>?) =
        mapListSection(src?.values?.toList())

    private fun mapListSection(src: List<HomeResponse?>?): List<CategoryDom> {
        return src?.map {
            CategoryDom(
                id = it?.id ?: "0",
                idParent = it?.id ?: "0",
                title = it?.name ?: "",
                link = it?.img ?: "",
                src = 0,
                includeSections = if (it?.includeSections?.isNotEmpty() == true) mapListSection(it.includeSections?.values?.toList()) else emptyList()
            )
        } ?: emptyList()
    }

}