package com.simrussia.mappers.home

import com.simrussa.models.home.CategoryDom
import com.simrussa.remote.models.home.HomeResponse
import com.simrussia.mappers.base.ResultMapper
import javax.inject.Inject

class HomeMapper @Inject constructor(
) : ResultMapper<List<HomeResponse>, List<CategoryDom>>() {
    override fun mapSuccessResult(src: List<HomeResponse>?): List<CategoryDom> {
        return src?.map {
            CategoryDom(
                id = it.id ?: "0",
                idParent = it.id ?: "0",
                title = it.name ?: "",
                link = it.img ?: "",
                src = 0,
                mapSuccessResult(it.includeSections?.values?.toList())
            )
        } ?: emptyList()
    }
}