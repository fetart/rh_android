package com.simrussia.mappers.authorization

import com.simrussa.remote.BaseResponse
import com.simrussia.mappers.base.FullResultMapper
import javax.inject.Inject

class PhoneModelMapper @Inject constructor(
) : FullResultMapper<Map<Any, Any>, String>() {
    override fun mapSuccessResult(src: BaseResponse<Map<Any, Any>>): String =
        src.toString()

}