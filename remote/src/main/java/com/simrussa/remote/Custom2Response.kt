package com.simrussa.remote

open class BaseResp<Response>(
    val data: Response?,
    val success: Boolean? = null,
    val message: String? = null
)