package com.simrussa.remote.models.home

import com.google.gson.annotations.SerializedName

data class HomeResponse(
    @SerializedName("id") val id: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("url") val url: String?,
    @SerializedName("count_products") val countProducts: String?,
    @SerializedName("country") val country: String?,
    @SerializedName("favorite") val favorite: Boolean?,
    @SerializedName("img") val img: String?,
    @SerializedName("filter") val filter: List<FilterResponse?>?,
    @SerializedName("include_sections") val includeSections: Map<String, HomeResponse>?
)
