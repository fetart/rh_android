package com.simrussa.remote.models.auth

import com.google.gson.annotations.SerializedName

data class AuthorizationResponse(
    @SerializedName("access_token")
    val accessToken: String?,
    @SerializedName("refresh_token")
    val refreshToken: String?,
)