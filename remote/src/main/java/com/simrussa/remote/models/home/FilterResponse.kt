package com.simrussa.remote.models.home

import com.google.gson.annotations.SerializedName

data class FilterResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("code") val code: String,
    @SerializedName("name") val name: String
)