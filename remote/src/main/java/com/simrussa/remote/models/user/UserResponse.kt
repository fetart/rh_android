package com.simrussa.remote.models.user

import com.google.gson.annotations.SerializedName

data class UserResponse(
    val id: String?,
    val name: String?,
    val surname: String?,
    val email: String?,
    @SerializedName("device_id")
    val deviceId: String?,
    @SerializedName("prof_customer")
    val profCustomer: Boolean?,
    val bonus: BonusResponse?,
)

data class BonusResponse(
    val prize: String?,
    val level: String?,
)