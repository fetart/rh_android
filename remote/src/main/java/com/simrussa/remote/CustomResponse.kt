//package com.simrussa.remote
//
//import kotlin.reflect.KClass
//import com.airbnb.lottie.model.content.CircleShape
//import com.google.gson.JsonDeserializationContext
//
//import com.google.gson.JsonDeserializer
//import com.google.gson.JsonElement
//import com.google.gson.JsonObject
//import java.lang.reflect.Type
//
//
//internal class ShapeDeserializer : JsonDeserializer<BaseResponse?> {
//    fun deserialize(
//        json: JsonElement, typeOfT: Type?,
//        context: JsonDeserializationContext
//    ): BaseResponse? {
//        val jsonObject: JsonObject = json.getAsJsonObject()
//        val type: JsonElement = jsonObject.get("type")
//        if (type != null) {
//            when (type.getAsString()) {
//                "rect" -> return context.deserialize(
//                    jsonObject,
//                    RectangleShape::class.java
//                )
//                "circle" -> return context.deserialize(
//                    jsonObject,
//                    CircleShape::class.java
//                )
//                "line" -> return context.deserialize(
//                    jsonObject,
//                    LineShape::class.java
//                )
//            }
//        }
//        return null
//    }
//}