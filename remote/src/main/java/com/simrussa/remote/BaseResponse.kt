package com.simrussa.remote

open class BaseResponse<Response>(
    val data: Response?,
    val success: Boolean? = null,
    val message: String
)
