package com.simrussa.remote

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class CustomBaseResponse : JsonDeserializer<BaseResponse<*>> {
    val gson = Gson()
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        ctx: JsonDeserializationContext
    ): BaseResponse<*> {
        val data = json.asJsonObject["data"]
        return if (data.isJsonArray)
            BaseResponse(null, json.asJsonObject["success"].asBoolean, "")
        else {
            val typeToken = TypeToken.get(typeOfT) as TypeToken<*>
            gson.fromJson(json, BaseResponse::class.java)
            //ctx.deserialize(json, BaseResponse::class.java)

        }
    }
}