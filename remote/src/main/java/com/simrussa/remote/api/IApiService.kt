package com.simrussa.remote.api

interface IApiService :
    IAuthorizationApiService, IApiHomeService
