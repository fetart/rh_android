package com.simrussa.remote.api

import com.simrussa.remote.BaseResponse
import com.simrussa.remote.models.home.HomeResponse
import com.simrussia.result.Result
import retrofit2.http.GET

interface IApiHomeService {
    @GET("catalog/sections")
    @JvmSuppressWildcards
    suspend fun getCatalog(): Result<BaseResponse<Map<String, HomeResponse>>>

    @GET("catalog/brands")
    @JvmSuppressWildcards
    suspend fun getBrands(): Result<BaseResponse<List<HomeResponse>>>
}