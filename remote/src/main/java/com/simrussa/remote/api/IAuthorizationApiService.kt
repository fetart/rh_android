package com.simrussa.remote.api

import com.simrussa.remote.BaseResponse
import com.simrussia.result.*
import com.simrussa.remote.models.auth.AuthorizationResponse
import com.simrussa.remote.models.user.UserResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface IAuthorizationApiService {
    @GET("system/notification/sms/send")
    @JvmSuppressWildcards
    suspend fun sendPhone(@Query("phone") phone: String): Result<BaseResponse<Map<Any, Any>>>

    @GET("system/notification/sms/check")
    @JvmSuppressWildcards
    suspend fun authorization(
        @Query("phone") phone: String,
        @Query("code") code: String
    ): Result<BaseResponse<Map<Any, Any>>>

    @POST("auth/logout")
    suspend fun deauthorization(): Result<BaseResponse<AuthorizationResponse>>

    @GET("user")
    suspend fun getInfoUser(@Query("phone") phone: String): Result<BaseResponse<UserResponse>>
}