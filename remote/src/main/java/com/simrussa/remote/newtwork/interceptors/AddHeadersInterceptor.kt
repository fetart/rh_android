package com.simrussa.remote.newtwork.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import com.simrussa.persistance.ApplicationPreferences
import java.util.*

class AddHeadersInterceptor(
    private val prefs: ApplicationPreferences,
    private val version: String
) : Interceptor {
    private val generateUUID = UUID.randomUUID().toString()

    companion object {
        private const val X_USER_PLATFORM_HEADER = "X-User-Platform"
        private const val SECRET_KEY = "wYDkhhejSaZjP5IUATkphP846cJZ6D5x"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = with(chain.request().newBuilder()) {
            addHeader(X_USER_PLATFORM_HEADER, "android")
            addHeader("Authorization", "Bearer $SECRET_KEY")

            // val token = prefs.accessToken

            build()
        }
        return chain.proceed(newRequest)
    }
}