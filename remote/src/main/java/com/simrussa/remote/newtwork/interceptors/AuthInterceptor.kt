package com.simrussa.remote.newtwork.interceptors

import com.google.gson.Gson
import com.simrussa.persistance.ApplicationPreferences
import okhttp3.*

class AuthInterceptor(
    private val prefs: ApplicationPreferences,
    private val gson: Gson,
    private val baseUrl: String,
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        synchronized(this) {

            return chain.proceed(chain.request())
        }
    }

    private fun checkErrorCode(code: Int): Boolean {
        if (code == 401) return true
        return false
    }




    companion object {
        const val PATH_REFRESH = "auth/tokenRefresh"
        const val PATH_LOGOUT = "auth"
    }
}
