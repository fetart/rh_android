package com.simrussa.remote.newtwork

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.simrussa.remote.BaseResponse
import com.simrussa.remote.BuildConfig
import com.simrussa.remote.CustomBaseResponse
import com.simrussa.remote.adapter.ItemTypeAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.simrussa.remote.newtwork.interceptors.AddHeadersInterceptor
import com.simrussa.remote.newtwork.interceptors.AuthInterceptor
import com.sumrussia.retrofit_result.ResultAdapterFactory
import java.util.concurrent.TimeUnit

object ApiServiceFactory {

    private const val DEFAULT_TIMEOUT_IN_SECONDS = 30L

    fun createOkHttpClient(
        addHeadersInterceptor: AddHeadersInterceptor,
        authInterceptor: AuthInterceptor
    ): OkHttpClient {
        return with(OkHttpClient.Builder()) {
            connectTimeout(DEFAULT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            readTimeout(DEFAULT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            writeTimeout(DEFAULT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            addInterceptor(addHeadersInterceptor)
            addInterceptor(authInterceptor)
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            }
            build()
        }
    }

    fun createRetrofit(baseUrl: String, client: OkHttpClient, gson: Gson): Retrofit {
        return with(Retrofit.Builder()) {
            baseUrl(baseUrl)
            client(client)
            addConverterFactory(GsonConverterFactory.create(gson))
            addCallAdapterFactory(ResultAdapterFactory())
            build()
        }
    }

    fun createGson(): Gson = GsonBuilder()
        .disableHtmlEscaping()
        .setPrettyPrinting()
        .registerTypeAdapterFactory(ItemTypeAdapterFactory())
        //.registerTypeAdapter(BaseResponse::class.java, CustomBaseResponse())
        .create()

}