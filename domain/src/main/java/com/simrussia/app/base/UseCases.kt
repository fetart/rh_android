package com.simrussia.app.base

import com.simrussia.result.Result
import com.simrussia.result.isSuccess

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

abstract class FlowResultUseCase<T> {
    operator fun invoke(): Flow<Result<T>> = flow {
        val result = retrieveData()
        if (result.isSuccess()) {
            emit(result)
        } else {
            emit(result)
        }
    }

    abstract suspend fun retrieveData(): Result<T>
}

abstract class FlowResultWithParamsUseCase<Param, Data> {
    operator fun invoke(params: Param): Flow<Result<Data>> = flow {
        emit(retrieveData(params))
    }

    abstract suspend fun retrieveData(params: Param): Result<Data>
}

abstract class FlowResultWithDoubleParamsUseCase<Param1, Param2, Data> {
    operator fun invoke(params1: Param1, params2: Param2): Flow<Result<Data>> = flow {
        emit(retrieveData(params1, params2))
    }

    abstract suspend fun retrieveData(params1: Param1, params2: Param2): Result<Data>
}
