package com.simrussia.app.interactor

import com.simrussia.repositories.authorization.IAuthorizationRepository
import javax.inject.Inject

class UserInteractor @Inject constructor(
    private val authorizationRepo: IAuthorizationRepository
) {
    suspend fun isAuthUser(): Boolean {
        return authorizationRepo.isAuth()
    }

    suspend fun setUserInfo(info: String?) {
        authorizationRepo.setAuthData(info)
    }

    suspend fun getUserInfo(): String? {
        return authorizationRepo.getUserInfo()
    }
}