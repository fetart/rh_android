package com.simrussia.app.usecase

import com.simrussa.models.home.CategoryDom
import com.simrussa.models.home.HomeDom
import com.simrussia.app.base.FlowResultUseCase
import com.simrussia.mappers.home.CatalogMapper
import com.simrussia.mappers.home.HomeMapper
import com.simrussia.repositories.home.ContentHomeRepository
import javax.inject.Inject

class ContentCatalogUseCase @Inject constructor(
    private val catalogRepo: ContentHomeRepository,
    private val catalogMapper: CatalogMapper
) : FlowResultUseCase<List<CategoryDom>>() {
    override suspend fun retrieveData() = catalogMapper.map(catalogRepo.getCatalog())
}