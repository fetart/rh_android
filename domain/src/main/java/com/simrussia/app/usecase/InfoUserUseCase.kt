package com.simrussia.app.usecase

import com.simrussa.models.user.UserDom
import com.simrussia.app.base.FlowResultWithParamsUseCase
import com.simrussia.repositories.authorization.IAuthorizationRepository
import com.simrussia.mappers.user.UserMapper
import javax.inject.Inject

class InfoUserUseCase @Inject constructor(
    private val authorizationRepo: IAuthorizationRepository,
    private val userMapper: UserMapper
) : FlowResultWithParamsUseCase<String, UserDom>() {
    override suspend fun retrieveData(params: String) =
        userMapper.map(authorizationRepo.getInfoUser(params))
}