package com.simrussia.app.usecase

import com.simrussia.app.base.FlowResultWithParamsUseCase
import com.simrussia.repositories.authorization.IAuthorizationRepository
import com.simrussia.mappers.authorization.PhoneModelMapper
import javax.inject.Inject

class SendPhoneUseCase @Inject constructor(
    private val authorizationRepo: IAuthorizationRepository,
    private val phoneModelMapper: PhoneModelMapper
) : FlowResultWithParamsUseCase<String, String>() {
    override suspend fun retrieveData(params: String) =
        phoneModelMapper.map(authorizationRepo.sendPhone(params))

}