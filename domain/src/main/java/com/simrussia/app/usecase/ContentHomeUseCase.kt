package com.simrussia.app.usecase

import com.simrussa.models.home.CategoryDom
import com.simrussia.app.base.FlowResultUseCase
import com.simrussia.mappers.home.HomeMapper
import com.simrussia.repositories.home.ContentHomeRepository
import javax.inject.Inject

class ContentHomeUseCase @Inject constructor(
    private val catalogRepo: ContentHomeRepository,
    private val cartMapper: HomeMapper
) : FlowResultUseCase<List<CategoryDom>>() {
    override suspend fun retrieveData() = cartMapper.map(catalogRepo.getBrands())
}