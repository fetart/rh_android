package com.simrussia.app.usecase

import com.simrussia.app.base.FlowResultWithDoubleParamsUseCase
import com.simrussia.app.base.FlowResultWithParamsUseCase
import com.simrussia.repositories.authorization.IAuthorizationRepository
import com.simrussia.mappers.authorization.PhoneModelMapper
import javax.inject.Inject

class AuthorizationLoginInUseCase @Inject constructor(
    private val authorizationRepo: IAuthorizationRepository,
    private val phoneModelMapper: PhoneModelMapper
) : FlowResultWithDoubleParamsUseCase<String, String, String>() {
    override suspend fun retrieveData(params1: String, params2: String) =
        phoneModelMapper.map(authorizationRepo.authorization(params1, params2))

}