package com.simrussa.persistance

import android.content.SharedPreferences
import android.util.Log
import java.util.*
import javax.inject.Inject

class ApplicationPreferences @Inject constructor(
    private val prefs: SharedPreferences
) {

    private companion object {
        const val DEVICE_UID = "device_uid"
        const val ACCESS_TOKEN = "access_token"
        const val ORDER_STEP = "order_step"
        const val REFRESH_TOKEN = "refresh_token"
        const val SIGN_IN = "login_in"
        const val START_APP = "start_app"
        const val ORDER_START = "order_start"
        const val CITY = "city"
        const val FAVORITES = "favorites"
        const val FILTERS = "filters"
        const val USER_INFO = "user_info"
        const val COUNTER = "counter"
        const val HOME = "home"
    }

    var deviceUid: String
        get() {
            var uuid = prefs.getString(DEVICE_UID, null)
            if (uuid.isNullOrBlank()) {
                uuid = UUID.randomUUID().toString()
                putString(DEVICE_UID, uuid)
            }
            return uuid
        }
        set(value) = putString(DEVICE_UID, value)

    var isOrderStart: Boolean
        get() =
            prefs.getBoolean(ORDER_START, false)
        set(value) =
            putBoolean(ORDER_START, value)

    var isFirstStartAPP: Boolean
        get() = prefs.getBoolean(START_APP, true)
        set(value) = putBoolean(START_APP, value)

    var signIn: Boolean
        get() = prefs.getBoolean(SIGN_IN, false)
        set(value) = putBoolean(SIGN_IN, value)

    var orderStep: String?
        get() = prefs.getString(ORDER_STEP, null)
        set(value) = putString(ORDER_STEP, value)

    var homeConstructor: String?
        get() = prefs.getString(HOME, null)
        set(value) = putString(HOME, value)

    var accessToken: String?
        get() = prefs.getString(ACCESS_TOKEN, null)
        set(value) = putString(ACCESS_TOKEN, value)

    var userInfo: String?
        get() = prefs.getString(USER_INFO, null)
        set(value) = putString(USER_INFO, value)
    var counter: Int
        get() = prefs.getInt(COUNTER, 60)
        set(value) = putInt(COUNTER, value)

    var refreshToken: String?
        get() = prefs.getString(REFRESH_TOKEN, null)
        set(value) = putString(REFRESH_TOKEN, value)

    var cityRaw: String?
        get() = prefs.getString(CITY, null)
        set(value) = putString(CITY, value)

    var filters: String?
        get() = prefs.getString(FILTERS, null)
        set(value) = putString(FILTERS, value)

    var userFavorite: String?
        get() = prefs.getString(FAVORITES, null)
        set(value) = putString(FAVORITES, value)

    fun putString(key: String, value: String?) {
        with(prefs.edit()) {
            this.putString(key, value)
            apply()
        }
    }

    fun putInt(key: String, value: Int) {
        with(prefs.edit()) {
            this.putInt(key, value)
            apply()
        }
    }

    fun putBoolean(key: String, value: Boolean) {
        with(prefs.edit()) {
            this.putBoolean(key, value)
            apply()
        }
    }

    fun getString(key: String) =
        prefs.getString(key, null)


    fun reset() {
        prefs.edit()
            .clear()
            .apply()
    }

    fun remove(categoryId: String) {
        prefs.edit()
            .remove(categoryId)
            .apply()
    }

}