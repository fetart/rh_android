package com.simrussa.models.events

sealed class KeyboardEvent {
    object ShowKeyboard : KeyboardEvent()
    object HideKeyboard : KeyboardEvent()
}