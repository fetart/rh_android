package com.simrussa.models.events

sealed class NavigationEvent {
    object NeedPopEvent : NavigationEvent()
    data class Navigate(
        val direction: Any,
        val extras: Any? = null
    ) : NavigationEvent()
}
