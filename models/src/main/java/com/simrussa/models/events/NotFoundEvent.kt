package com.simrussa.models.events

sealed class NotFoundEvent {
    data class ShowNotFound(val q: String, val data: Any? = null) : NotFoundEvent()
    object HideNotFound : NotFoundEvent()
}