package com.simrussa.models.web

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class WebViewParam(
    val url: String
) : Parcelable