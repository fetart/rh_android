package com.simrussa.models.alert

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AlertParams(
    val title: String? = null,
    val subtitle: String? = null,
    val firstAction: AlertAction? = null,
    val secondAction: AlertAction? = null,
    val isBlack: Boolean = true,
    val showNotificationIcon: Boolean = true,
    val showCloseIcon: Boolean = true,
    val type: AlertType
) : Parcelable