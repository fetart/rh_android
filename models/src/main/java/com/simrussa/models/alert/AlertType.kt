package com.simrussa.models.alert

enum class AlertType {
    Modal, Popup, Message, Error
}