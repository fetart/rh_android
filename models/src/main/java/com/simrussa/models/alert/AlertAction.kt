package com.simrussa.models.alert

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AlertAction(
    val title: String,
    val action: () -> Unit
) : Parcelable