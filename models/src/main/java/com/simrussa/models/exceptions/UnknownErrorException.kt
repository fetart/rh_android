package com.simrussa.models.exceptions

import java.lang.Exception

class UnknownErrorException : Exception()