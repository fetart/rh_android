package com.simrussa.models.home

data class HomeDom(
    val head: HeadDom,
    val categories: List<CategoryDom>,
    val brands: List<CategoryDom>,
)


