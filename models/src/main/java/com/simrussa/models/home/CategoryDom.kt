package com.simrussa.models.home

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryDom(
    val id: String,
    val idParent: String,
    val title: String,
    val link: String,
    var src: Int,
    var includeSections: List<CategoryDom> = emptyList(),
) : Parcelable