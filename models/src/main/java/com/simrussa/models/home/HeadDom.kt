package com.simrussa.models.home

data class HeadDom(
    val name: String,
    val status: String,
    val bonus: String,
)