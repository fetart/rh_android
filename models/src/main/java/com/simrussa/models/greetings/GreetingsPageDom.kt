package com.simrussa.models.greetings

data class GreetingsPageDom(
    val id: Int,
    val title: String,
    val description: String,
    val image: Int,
)