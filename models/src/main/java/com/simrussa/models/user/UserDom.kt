package com.simrussa.models.user


data class UserDom(
    val id: String,
    val name: String,
    val surname: String,
    val email: String,
    val deviceId: String,
    val profCustomer: Boolean,
    val bonus: BonusDom,
)

data class BonusDom(
    val prize: String,
    val level: String,
)