package com.simrussa.models.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class TypeUser() : Parcelable {
    ANON, NO_ANON, PROF_CALL, PROF_MESSAGE, NO_PROF_CALL, MO_PROF_MESSAGE
}