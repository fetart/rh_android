package com.simrussia.app.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

open class HorizontalSpaceItemDecoration(
    protected val sizeOffsetInDp: Int
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (isCanAddOffset(view, parent)) {
            outRect.left = SizeUtils.dp2px(sizeOffsetInDp)
            outRect.right = SizeUtils.dp2px(sizeOffsetInDp)
        }
    }

    protected open fun isCanAddOffset(view: View, parent: RecyclerView): Boolean = true

}