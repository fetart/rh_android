package com.simrussia.app.util

import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.simrussia.app.extensions.toPx

@RequiresApi(Build.VERSION_CODES.N)
class GridHorizontalSpaceItemDecoration(
    private val context: Context
) : HorizontalSpaceItemDecoration(16) {

    private val screenWidth: Int by lazy { SizeUtils.screenWidth(context, false) }
    private val halfScreenWidth: Int by lazy { screenWidth / 2 }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (isCanAddOffset(view, parent)) {
            when (typePosition(view, parent)) {
                PositionGrid.LEFT -> {
                    outRect.top = 6.toPx()
                    outRect.bottom = 6.toPx()
                    outRect.left = SizeUtils.dp2px(sizeOffsetInDp)
                    outRect.right = SizeUtils.dp2px(sizeOffsetInDp / 3)
                }
                PositionGrid.RIGHT -> {
                    outRect.top = 6.toPx()
                    outRect.bottom = 6.toPx()
                    outRect.left = SizeUtils.dp2px(sizeOffsetInDp / 3)
                    outRect.right = SizeUtils.dp2px(sizeOffsetInDp)
                }
                PositionGrid.CENTER -> {
                    outRect.top = 6.toPx()
                    outRect.bottom = 6.toPx()
                    outRect.left = SizeUtils.dp2px(sizeOffsetInDp / 3)
                    outRect.right = SizeUtils.dp2px(sizeOffsetInDp / 3)
                }
                else -> {

                }
            }
        }
    }

    override fun isCanAddOffset(view: View, parent: RecyclerView): Boolean {
        val layoutManager = parent.layoutManager as? GridLayoutManager ?: return false
        val itemPosition = layoutManager.getPosition(view)
        val spanSize = layoutManager.spanSizeLookup.getSpanSize(itemPosition)
        return spanSize == 1
    }

    private fun typePosition(view: View, parent: RecyclerView): PositionGrid {
        // TODO Реализовать проверки для всех остальных типов холдеров
        val layoutManager = parent.layoutManager as? GridLayoutManager ?: return PositionGrid.LIST
        val itemPosition = layoutManager.getPosition(view)
        Log.e("isLeftGridItem", (itemPosition % 3).toString())
        //val spanSize = layoutManager.spanSizeLookup.getSpanSize(itemPosition)
        return when (itemPosition % 3) {
            PositionGrid.LEFT.position -> {
                PositionGrid.LEFT
            }
            PositionGrid.CENTER.position -> {
                PositionGrid.CENTER
            }
            PositionGrid.RIGHT.position -> {
                PositionGrid.RIGHT
            }
            else -> PositionGrid.CENTER
        }

    }

    enum class PositionGrid(val position: Int) {
        LEFT(1), CENTER(2), RIGHT(0), LIST(-1)

    }
}