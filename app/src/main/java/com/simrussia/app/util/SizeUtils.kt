package com.simrussia.app.util

import android.content.Context
import android.content.res.Resources
import android.graphics.Point
import android.os.Build
import androidx.annotation.Px
import androidx.annotation.RequiresApi

object SizeUtils {

    private var lastKnownWidth: Int = -1

    fun refreshScreenSize(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            screenWidth(context, refresh = true)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    fun screenWidth(context: Context, refresh: Boolean = false): Int {
        if (refresh || lastKnownWidth == -1) {
            val point = Point()
            context.display?.getRealSize(point)
            lastKnownWidth = point.x
        }
        return lastKnownWidth
    }

    @Px
    fun dp2px(dpValue: Int): Int {
        val scale = Resources.getSystem().displayMetrics.density
        return ((dpValue * scale + 0.5f).toInt())
    }

}