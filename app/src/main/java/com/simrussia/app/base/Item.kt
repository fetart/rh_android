package com.simrussia.app.base

import androidx.viewbinding.ViewBinding
import com.xwray.groupie.viewbinding.BindableItem

abstract class Item<T : ViewBinding> : BindableItem<T>()