package com.simrussia.app.base

import androidx.annotation.LayoutRes
import androidx.viewbinding.ViewBinding

abstract class BaseBindingItem<T : ViewBinding>(
    @LayoutRes private val layoutId: Int,
    private val clickable: Boolean = true,
    private val longClickable: Boolean = false
) : Item<T>() {
    override fun getLayout(): Int = layoutId
    override fun isClickable(): Boolean = clickable
    override fun isLongClickable(): Boolean = longClickable
}