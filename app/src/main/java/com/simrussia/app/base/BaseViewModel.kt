package com.simrussia.app.base

import android.content.Intent
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import androidx.navigation.Navigator
import com.simrussa.models.alert.AlertParams
import com.simrussa.models.alert.AlertType
import com.simrussa.models.events.KeyboardEvent
import com.simrussa.models.events.ViewEvent
import com.simrussa.models.exceptions.UnknownErrorException
import com.simrussa.models.events.NavigationEvent
import com.simrussia.app.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext
import com.simrussia.app.R
import com.simrussia.result.HttpException
import com.simrussia.result.Result
import com.simrussia.result.asFailure
import com.simrussia.result.asSuccess
import com.simrussia.result.isSuccess
import com.simrussia.utils.ResourceProvider
import kotlinx.coroutines.flow.MutableSharedFlow
import java.net.UnknownHostException

@HiltViewModel
open class BaseViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var resourceProvider: ResourceProvider
    protected var isShowBlackAlerts: Boolean = true

    protected val alertsSharedFlow = MutableSharedFlow<AlertParams>()
    val alertsFlow: Flow<AlertParams> get() = alertsSharedFlow


    private val _navigationMutableLiveData = SingleLiveEvent<NavigationEvent>()
    val navigationEventLiveData: LiveData<NavigationEvent> get() = _navigationMutableLiveData

    private val _intentMutableFlow: MutableSharedFlow<Intent> = MutableSharedFlow()
    val intentFlow: Flow<Intent> get() = _intentMutableFlow

    protected var _keyboardMutableFlow = MutableSharedFlow<KeyboardEvent>()
    val keyboardFlow: Flow<KeyboardEvent> get() = _keyboardMutableFlow

    private var _popBackStackFlow = MutableSharedFlow<Boolean>()
    val popBackStackFlow: Flow<Boolean> get() = _popBackStackFlow


    private val loadingMutableFlow =
        MutableStateFlow<ViewEvent.Loading>(ViewEvent.Loading.InitialEvent)
    val loadingFlow: Flow<ViewEvent.Loading> get() = loadingMutableFlow

    private val alertVisibilityMutableFlow =
        MutableStateFlow<ViewEvent.Alert>(ViewEvent.Alert.HideAlert)
    val alertVisibilityFlow: Flow<ViewEvent.Alert> get() = alertVisibilityMutableFlow


    protected fun NavDirections.emit(extras: Navigator.Extras? = null) {
        _navigationMutableLiveData.postValue(NavigationEvent.Navigate(this, extras))
    }

    protected fun NavigationEvent.NeedPopEvent.emit() {
        _navigationMutableLiveData.postValue(this)
    }

    protected fun launchInVMScope(
        context: CoroutineContext = Dispatchers.IO,
        block: suspend CoroutineScope.() -> Unit
    ) {
        viewModelScope.launch(context = context, block = block)
    }

    protected suspend fun <T> Result<T>.getIfSuccessNoError(): T? {
        if (isSuccess()) {
            hideLoading()
            return when (asSuccess()) {
                is Result.Success.Empty -> {
                    Any() as? T?
                }
                else -> {
                    asSuccess().value
                }
            }
        }
        return null
    }


    protected suspend fun <T> Result<T>.getIfSuccess(): T? {
        if (isSuccess()) {
            hideLoading()
            return when (asSuccess()) {
                is Result.Success.Empty -> {
                    Any() as? T?
                }
                else -> {
                    asSuccess().value
                }
            }
        } else {
            processError(asFailure())
        }
        return null
    }

    protected suspend inline fun <T> Flow<Result<T>>.collectSuccess(crossinline action: suspend (value: T) -> Unit) {
        collect { result ->
            if (result.isSuccess()) {
                hideLoading()
                when (result.asSuccess()) {
                    is Result.Success.Empty -> {
                        action(Any() as T)
                    }
                    else -> {
                        with(result.asSuccess()) {
                            action(value)
                        }
                    }
                }
            } else {
                hideLoading()
                processError(result.asFailure())
            }
        }
    }

    fun processError(
        error: Result.Failure<out Throwable>,
        showAlert: Boolean = true
    ) {
        //Timber.e(error.error)
        launchInVMScope {
            hideLoading()
            if (showAlert) {
                try {
                    val params = createAlertForError(error)
                    alertsSharedFlow.emit(params)
                } catch (e: UninitializedPropertyAccessException) {
                    //ignore
                }
            }
        }
    }

    private fun createAlertForError(error: Result.Failure<*>): AlertParams {
        return when (error) {
            is Result.Failure.Error -> {
                createAlertParamsFromThrowable(error.error)
            }
            is Result.Failure.HttpError -> {
                // TODO обработка ошибок от сервера
                createAlertParamsForUnknownError()
            }
            is Result.Failure.ErrorMessages -> {
                createAlertParamsForUnknownError()
            }
        }
    }

    private fun createAlertParamsForUnknownError(): AlertParams {
        return AlertParams(
            getString(R.string.error_unknown_title),
            getString(R.string.error_unknown_subtitle),
            isBlack = isShowBlackAlerts,
            showNotificationIcon = true,
            type = AlertType.Popup
        )
    }

    private fun createAlertParamsFromThrowable(t: Throwable?): AlertParams {
        return when (t) {
            is UnknownErrorException ->
                createAlertParamsForUnknownError()
            is UnknownHostException ->
                createAlertParamsForUnknownHost()
            is HttpException ->
                createAlertParamsForUnknownHost()
            else -> createAlertParamsForUnknownError()
        }
    }


    private fun createAlertParamsForUnknownHost(): AlertParams {
        return AlertParams(
            getString(R.string.error_connection_title),
            getString(R.string.error_connection_subtitle),
            isBlack = isShowBlackAlerts,
            showNotificationIcon = true,
            type = AlertType.Popup
        )
    }

    protected fun getString(@StringRes id: Int): String = resourceProvider.getString(id)
    protected fun getColor(@ColorRes id: Int): Int = resourceProvider.getColor(id)

    suspend fun hideLoading() {
        loadingMutableFlow.emit(ViewEvent.Loading.HideLoading)
    }

    protected suspend fun showLoading() {
        loadingMutableFlow.emit(ViewEvent.Loading.ShowLoading)
    }

    suspend fun hideAlert() {
        alertVisibilityMutableFlow.emit(ViewEvent.Alert.HideAlert)
    }

    protected suspend fun showAlert() {
        alertVisibilityMutableFlow.emit(ViewEvent.Alert.ShowAlert)
    }


    protected fun launchMain(
        context: CoroutineContext = Dispatchers.Main,
        block: suspend CoroutineScope.() -> Unit
    ) {
        viewModelScope.launch(context = context, block = block)
    }
}