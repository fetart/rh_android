package com.simrussia.app.base

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.animation.OvershootInterpolator
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.Navigator
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.ladygentleman.app.utils.KeyboardUtils
import com.simrussa.models.alert.AlertParams
import com.simrussa.models.alert.AlertType
import com.simrussa.models.events.KeyboardEvent
import com.simrussa.models.events.ViewEvent
import com.simrussia.app.BuildConfig
import com.simrussia.app.R
import com.simrussia.app.databinding.LoadingContainerBinding
import com.simrussa.models.events.NavigationEvent
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import androidx.annotation.ColorRes
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import com.simrussia.app.extensions.*


abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {
    protected val loadingBinding: LoadingContainerBinding by viewBinding(
        LoadingContainerBinding::bind,
        R.id.loading_container
    )

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        transparentStatusBar()
        //transparent()
    }

    private fun whiteStatusBar() {
        setStatusBarColor(R.color.white)
    }

    private fun transparent() {
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        activity?.window?.statusBarColor = Color.TRANSPARENT
    }

    private fun transparentStatusBar() {
        activity?.window.apply {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity?.window?.decorView?.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                activity?.window?.decorView?.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            activity?.window?.statusBarColor = Color.TRANSPARENT
        }
    }

    protected fun collectDefaultEvents(
        viewModel: BaseViewModel,
        collectLoading: Boolean = true
    ) {
        collectAlerts(viewModel.alertsFlow)
        observeNavigationEvent(viewModel.navigationEventLiveData)
        collectIntent(viewModel.intentFlow)
        collectKeyboardEvent(viewModel.keyboardFlow)
        collectPopBackStack(viewModel.popBackStackFlow)
        collectVisibilityAlert(viewModel.alertVisibilityFlow)
        if (collectLoading) {
            collectLoading(viewModel.loadingFlow)
        }
    }

    protected fun getColor(@ColorRes id: Int) = ContextCompat.getColor(requireContext(), id)


    private fun collectLoading(flow: Flow<ViewEvent.Loading>) {
        launchInMain { flow.collect { event -> processLoadingEvent(event) } }
    }

    private fun collectVisibilityAlert(flow: Flow<ViewEvent.Alert>) {
        launchInMain { flow.collect { event -> processVisibilityAlertEvent(event) } }
    }

    protected open fun processVisibilityAlertEvent(alertVisibility: ViewEvent.Alert) {
        with(loadingBinding) {
            when (alertVisibility) {
                is ViewEvent.Alert.ShowAlert -> {
                    root.visible()
                    blade.visible()
                    loading.gone()
                }
                is ViewEvent.Alert.HideAlert -> {
                    blade.gone()
                }
            }
        }
    }

    protected open fun processLoadingEvent(loading: ViewEvent.Loading) {
        with(loadingBinding) {
            when (loading) {
                is ViewEvent.Loading.ShowLoading -> {
                    root.visible()
                    loadingBinding.loading.visible()
                }
                is ViewEvent.Loading.HideLoading -> {
                    loadingBinding.loading.gone()
                }
                else -> {}
            }
        }
    }

    private fun showTopAlertAnimation(title: String?, isError: Boolean = true) {
        with(loadingBinding) {
            if (isError) blade.setBackgroundResource(R.drawable.bg_error_dialog)
            else blade.setBackgroundResource(R.drawable.bg_message_dialog)

            titleError.text = title
            root.visible()
            blade.visible()
            var bias = 100.toPx().toFloat()
            if (!isError)
                bias = 150.toPx().toFloat()

            val hideAnimator =
                ObjectAnimator.ofFloat(blade, "TranslationY", bias * -1).apply {
                    startDelay = 1500
                    repeatCount = 0
                    duration = 1500
                }
            val showAnimator =
                ObjectAnimator.ofFloat(blade, "TranslationY", bias).apply {
                    repeatCount = 0
                    interpolator = OvershootInterpolator(2f)
                    duration = 1000
                }

            AnimatorSet().apply {
                play(showAnimator).before(hideAnimator)
                start()
            }.doOnEnd {
                launchInMain {
                    blade.gone()
                    titleError.text = ""
                }
            }
        }
    }

    private fun hideTopAlertAnimation() {
        loadingBinding.blade.animate()
            .setStartDelay(1000L)
            .setDuration(1500L)
            .translationYBy(loadingBinding.blade.height.toFloat() * -1)
            .withEndAction {
                launchInMain {
                    loadingBinding.titleError.text = ""
                    processLoadingEvent(ViewEvent.Loading.HideLoading)
                }
            }
    }

    private fun collectPopBackStack(popBackStackFlow: Flow<Boolean>) {
        launchInMain {
            popBackStackFlow.collect {
                if (it)
                    findNavController().popBackStack()
            }
        }
    }

    private fun collectKeyboardEvent(flow: Flow<KeyboardEvent>) {
        launchInMain { flow.collect { event -> processKeyboardEvent(event) } }
    }

    private fun processKeyboardEvent(event: KeyboardEvent) {
        if (view != null) {
            when (event) {
                KeyboardEvent.ShowKeyboard -> KeyboardUtils.showKeyboard(requireView())
                KeyboardEvent.HideKeyboard -> KeyboardUtils.hideKeyboard(requireView())
            }
        }
    }

    private fun collectIntent(flow: Flow<Intent>) {
        launchInMain { flow.collect { startActivity(it) } }
    }

    private fun collectAlerts(flow: Flow<AlertParams>) {
        launchInMain { flow.collect { params -> showAlert(params) } }
    }

    protected fun showAlert(params: AlertParams) {
        try {
            when (params.type) {
                AlertType.Message -> {
                    showTopAlertAnimation(params.title, false)
                }
                AlertType.Error -> {
                    showTopAlertAnimation(params.title)
                }
                else -> {
                    showTopAlertAnimation(params.title)
                }
            }

        } catch (e: Exception) {

        }
    }


    fun setStatusBarColor(colorId: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity?.window?.statusBarColor =
                resources.getColor(colorId, activity?.theme)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity?.window?.statusBarColor = Color.BLACK
        }
    }


    private fun collectResultListener(resultListenerFlow: Flow<Map<String, Any>>) {
        launchInMain { resultListenerFlow.collect { it -> getResultListener(it) } }
    }

    open fun getResultListener(map: Map<String, Any>) {}

    private fun collectNavigationEvents(flow: Flow<NavigationEvent>) {
        launchInMain {
            flow.collect { event ->
                processNavigationEvent(event)
            }
        }
    }

    private fun observeNavigationEvent(liveData: LiveData<NavigationEvent>) {
        liveData.observe(
            viewLifecycleOwner,
            Observer { event -> processNavigationEvent(event) }
        )
    }

    protected open fun interceptNavDirections(directions: NavDirections): Boolean {
        return false
    }

    protected fun onBackPressed() {
        requireActivity().onBackPressed()
    }


    private fun processNavigationEvent(event: NavigationEvent) {
        when (event) {
            is NavigationEvent.Navigate -> {
                //От случайных двойных переходов итп
                try {
                    val directions = event.direction as NavDirections
                    if (!interceptNavDirections(directions)) {
                        val extras = event.extras
                        if (extras is Navigator.Extras) {
                            findNavController().navigate(directions, extras)
                        } else {
                            findNavController().navigate(directions)
                        }
                    }
                } catch (e: Exception) {
                    if (BuildConfig.DEBUG) e.printStackTrace()
                }
            }
            is NavigationEvent.NeedPopEvent -> {
                onBackPressed()
            }
        }
    }
}