package com.simrussia.app.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import com.simrussia.app.databinding.CustomButtonBinding

class CustomButton : CardView {
    private val customButtonBinding: CustomButtonBinding

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        context ?: throw NullPointerException("Context must not be empty")
        val inflater = LayoutInflater.from(context)
        customButtonBinding = CustomButtonBinding.inflate(inflater, this)
    }
}