package com.simrussia.app.viewmodel.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.simrussa.models.home.CategoryDom
import com.simrussa.models.home.HeadDom
import com.simrussia.app.base.BaseViewModel
import com.simrussa.models.home.HomeDom
import com.simrussa.models.user.UserDom
import com.simrussia.app.R
import com.simrussia.app.interactor.UserInteractor
import com.simrussia.app.items.*
import com.simrussia.app.usecase.ContentCatalogUseCase
import com.simrussia.app.usecase.ContentHomeUseCase
import com.simrussia.app.usecase.InfoUserUseCase
import com.simrussia.app.view.home.HomeFragmentDirections
import com.xwray.groupie.Group
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.withContext

class ShowcaseViewModel @AssistedInject constructor(
    private val brandsUseCase: ContentHomeUseCase,
    private val contentCatalogUseCase: ContentCatalogUseCase,
    private val infoUserUseCase: InfoUserUseCase,
    private val userInteractor: UserInteractor,
) : BaseViewModel() {
    private val _profileItems: MutableLiveData<List<Group>> =
        MutableLiveData(emptyList())
    val profileItems: LiveData<List<Group>> get() = _profileItems

    fun init() {
        launchMain {
            showLoading()
            val brands = getBrandsAsync()
            showLoading()
            val catalog = getCatalogAsync().addSrc()
            showLoading()
            val userInfo = getProfileAsync()
            showLoading()
            generationContent(
                HomeDom(
                    head = mapUserInfoToHeadItem(userInfo),
                    categories = catalog,
                    brands = brands
                )
            ).let {
                _profileItems.postValue(it)
            }
            hideLoading()
        }
    }


    private fun mapUserInfoToHeadItem(userInfo: UserDom?): HeadDom {
        return userInfo?.let {
            HeadDom(
                name = userInfo.name + " " + userInfo.surname,
                bonus = userInfo.bonus.prize ?: "",
                status = "Базовый статус"
            )
        } ?: HeadDom("", "", "")
    }

    private suspend fun getProfileAsync() = withContext(Dispatchers.IO)
    {
        var userInfo: UserDom? = null
        userInteractor.getUserInfo()?.let {
            infoUserUseCase(it).collect {
                it.getIfSuccessNoError().let {
                    userInfo = it
                }
            }
        }
        return@withContext userInfo
    }

    private suspend fun getBrandsAsync() = withContext(Dispatchers.IO)
    {
        var list = emptyList<CategoryDom>()
        brandsUseCase().collectSuccess {
            list = it.take(9)
        }
        return@withContext list
    }

    private suspend fun getCatalogAsync() = withContext(Dispatchers.IO)
    {
        var list = emptyList<CategoryDom>()
        contentCatalogUseCase().collectSuccess {
            list = it
        }
        return@withContext list
    }

    private fun generationContent(homeDom: HomeDom): List<Group> {
        val items = ArrayList<Group>()
        items += HeadHomeItem(homeDom.head)
        items += VerticalSpaceItem(19)
        items += TitleItem("Каталог RedHare Market")
        items += VerticalSpaceItem(2)
        homeDom.categories.forEach { items += CategoryItem(it, ::openCategory) }
        items += VerticalSpaceItem(20)
        items += TitleItem("Популярные бренды")
        items += VerticalSpaceItem(1)
        items += VerticalSpaceItem(3)
        homeDom.brands.forEach { items += CategoryItem(it) }
        items += VerticalSpaceItem(14)
        items += GrayButtonItem("ВСЕ БРЕНДЫ")
        items += VerticalSpaceItem(40)
        items += RedButtonItem("ПЕРЕЙТИ НА REDHARE MARKET")
        items += VerticalSpaceItem(130)
        return items
    }

    private fun openCategory(category: CategoryDom) {
        HomeFragmentDirections.actionHomeFragmentToCategoryFragment(category).emit()
    }

    private fun List<CategoryDom>.addSrc() =
        this.toMutableList().apply {
            if (size % 3 != 0) {
                add(size - 1, CategoryDom("-1", "", "", "", 0, emptyList()))
            }
            forEach {
                it.src = when (it.id) {
                    "1932" -> R.drawable.ic_1
                    "1938" -> R.drawable.ic_1938
                    "1944" -> R.drawable.ic_1944
                    "1950" -> R.drawable.ic_1950
                    "1979" -> R.drawable.ic_2
                    "2010" -> R.drawable.ic_2010
                    "2018" -> R.drawable.ic_2018
                    "2039" -> R.drawable.ic_2039
                    "2044" -> R.drawable.ic_2044
                    "2109" -> R.drawable.ic_2109
                    "2110" -> R.drawable.ic_2110
                    "2113" -> R.drawable.ic_2113
                    "2117" -> R.drawable.ic_2117
                    else -> R.drawable.ic_1
                }
            }
        }

    companion object {
        @Suppress("UNCHECKED_CAST")
        fun provideFactory(
            assistedFactory: AssistedFactory,
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        }
    }

    @dagger.assisted.AssistedFactory
    interface AssistedFactory {
        fun create(
        ): ShowcaseViewModel
    }
}

