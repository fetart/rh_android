package com.simrussia.app.viewmodel.brands.items

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import com.simrussa.models.home.CategoryDom
import com.simrussia.app.R
import com.simrussia.app.databinding.ItemSimpleListBinding

class BrandListItem(
    private val brand: CategoryDom,
    val size: Int = 0
) : SimpleListItem() {
    override fun bind(viewBinding: ItemSimpleListBinding, position: Int) {
        if (size > 0) {
            val sb = SpannableStringBuilder(brand.title)
            val bss = StyleSpan(android.graphics.Typeface.BOLD)
            sb.setSpan(
                bss,
                0,
                size,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
            viewBinding.listItemText.text = sb
        }else {
            viewBinding.listItemText.text = brand.title
        }
    }
}