package com.simrussia.app.viewmodel.brands.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemSimpleListBinding

abstract class SimpleListItem : BaseBindingItem<ItemSimpleListBinding>(VIEW_TYPE) {

    companion object {
        const val VIEW_TYPE = R.layout.item_simple_list
    }

    override fun initializeViewBinding(view: View): ItemSimpleListBinding {
        return ItemSimpleListBinding.bind(view)
    }
}