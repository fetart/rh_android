package com.simrussia.app.viewmodel.brands.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemLetterBinding

class LetterItem(
    private val letter: String?
) : BaseBindingItem<ItemLetterBinding>(R.layout.item_letter) {
    override fun bind(viewBinding: ItemLetterBinding, position: Int) {
        viewBinding.letterText.text = letter
    }

    override fun initializeViewBinding(view: View): ItemLetterBinding = ItemLetterBinding.bind(view)
}