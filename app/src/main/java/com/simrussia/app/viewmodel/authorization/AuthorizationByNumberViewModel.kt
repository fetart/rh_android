package com.simrussia.app.viewmodel.authorization

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.simrussa.models.alert.AlertParams
import com.simrussa.models.alert.AlertType
import com.simrussa.models.user.TypeUser
import com.simrussia.app.base.BaseViewModel
import com.simrussia.app.interactor.UserInteractor
import com.simrussia.app.usecase.SendPhoneUseCase
import com.simrussia.app.view.authorization.ProfileFragmentDirections
import com.xwray.groupie.Group
import dagger.assisted.AssistedInject
import kotlinx.coroutines.delay

class AuthorizationByNumberViewModel @AssistedInject constructor(
    private val sendPhoneUseCase: SendPhoneUseCase
) : BaseViewModel() {
    private val _profileItems: MutableLiveData<List<Group>> =
        MutableLiveData(emptyList())
    val profileItems: LiveData<List<Group>> get() = _profileItems

    fun sendNumber(number: String) {
        launchInVMScope {
            showLoading()
            sendPhoneUseCase(number).collectSuccess {
                openSendCode(number)
            }
        }
    }

    private fun createParams(): AlertParams {
        return AlertParams(
            "Ошика",
            "",
            isBlack = isShowBlackAlerts,
            showNotificationIcon = true,
            type = AlertType.Error
        )
    }

    private fun createMessageParams(): AlertParams {
        return AlertParams(
            "На указанный номер отправлено СМС с новым кодом\n",
            "",
            isBlack = isShowBlackAlerts,
            showNotificationIcon = true,
            type = AlertType.Message
        )
    }

    private fun openSendCode(number: String) {
        ProfileFragmentDirections.actionProfileFragmentToSendCodeFragment(
            number
        ).emit()
    }


    companion object {
        @Suppress("UNCHECKED_CAST")
        fun provideFactory(
            assistedFactory: AssistedFactory,
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        }
    }

    @dagger.assisted.AssistedFactory
    interface AssistedFactory {
        fun create(
        ): AuthorizationByNumberViewModel
    }
}