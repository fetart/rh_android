package com.simrussia.app.viewmodel.authorization

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.simrussa.models.alert.AlertParams
import com.simrussa.models.alert.AlertType
import com.simrussa.models.user.TypeUser
import com.simrussia.app.base.BaseViewModel
import com.simrussia.app.interactor.UserInteractor
import com.simrussia.app.usecase.AuthorizationLoginInUseCase
import com.simrussia.app.usecase.InfoUserUseCase
import com.simrussia.app.usecase.SendPhoneUseCase
import com.simrussia.app.view.authorization.AuthorizationCodeDialogFragmentArgs
import com.simrussia.app.view.authorization.AuthorizationCodeDialogFragmentDirections
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce

class AuthorizationCodeViewModel @AssistedInject constructor(
    @Assisted private val args: AuthorizationCodeDialogFragmentArgs,
    private val sendPhoneUseCase: SendPhoneUseCase,
    private val authorizationLoginInUseCase: AuthorizationLoginInUseCase,
    private val infoUserUseCase: InfoUserUseCase,
    private val userInteractor: UserInteractor,
) : BaseViewModel() {
    private val _stateError = MutableLiveData(false)
    val stateError: LiveData<Boolean> get() = _stateError

    private val _isAutho = MutableLiveData(false)
    val isAutho: LiveData<Boolean> get() = _isAutho

    fun sendCodeAgain() {
        launchInVMScope {
            showLoading()
            alertsSharedFlow.emit(createSendAgainParams())
            sendPhoneUseCase(args.phone).debounce(TIMING)
                .collectSuccess {
                    alertsSharedFlow.emit(createMessageParams())
                }
        }
    }

    fun onAuthorization(code: String) {
        launchInVMScope {
            showLoading()
            authorizationLoginInUseCase(args.phone, code).debounce(TIMING).collect {
                hideLoading()
                it.getIfSuccess()?.let {
                    getInfoUser(args.phone)
                } ?: showError()
            }
        }
    }

    private fun getInfoUser(phone: String) {
        launchInVMScope {
            showLoading()
            infoUserUseCase(phone).collect {
                userInteractor.setUserInfo(phone)
                it.getIfSuccess()?.let {
                    openWelcomeScreen(it.profCustomer, it.id.isEmpty())
                } ?: openWelcomeScreen(false, true)
            }
        }
    }

    private fun openWelcomeScreen(isProf: Boolean, isUserAnon: Boolean) {
        if (isProf)
            AuthorizationCodeDialogFragmentDirections.actionSendCodeFragmentToHomeFragment().emit()
        else {
            if (isUserAnon)
                AuthorizationCodeDialogFragmentDirections.actionSendCodeFragmentToUserWelcomeFragment(
                    TypeUser.ANON
                ).emit()
            else AuthorizationCodeDialogFragmentDirections.actionSendCodeFragmentToUserWelcomeFragment(
                TypeUser.NO_ANON
            ).emit()
        }
    }

    private fun createMessageParams(): AlertParams {
        return AlertParams(
            "На указанный номер отправлено СМС с новым кодом\n",
            "",
            isBlack = isShowBlackAlerts,
            showNotificationIcon = true,
            type = AlertType.Message
        )
    }

    private fun showError() {
        launchMain {
            alertsSharedFlow.emit(createParams())
        }
        _stateError.postValue(true)
    }

    private fun createSendAgainParams(): AlertParams {
        return AlertParams(
            "На указанный номер отправлено СМС с новым кодом",
            "",
            isBlack = isShowBlackAlerts,
            showNotificationIcon = true,
            type = AlertType.Message
        )
    }

    private fun createParams(): AlertParams {
        return AlertParams(
            "Неверный код",
            "",
            isBlack = isShowBlackAlerts,
            showNotificationIcon = true,
            type = AlertType.Popup
        )
    }

    @dagger.assisted.AssistedFactory
    interface AssistedFactory {
        fun create(args: AuthorizationCodeDialogFragmentArgs): AuthorizationCodeViewModel
    }

    companion object {
        const val TIMING = 1000L

        @Suppress("UNCHECKED_CAST")
        fun provideFactory(
            assistedFactory: AssistedFactory,
            args: AuthorizationCodeDialogFragmentArgs
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create(args) as T
            }
        }
    }

}