package com.simrussia.app.viewmodel.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.simrussia.app.base.BaseViewModel
import com.simrussa.models.home.HomeDom
import com.simrussia.app.interactor.UserInteractor
import com.simrussia.app.items.*
import com.simrussia.app.usecase.ContentCatalogUseCase
import com.simrussia.app.usecase.ContentHomeUseCase
import com.simrussia.app.usecase.InfoUserUseCase
import com.simrussia.app.view.authorization.AuthorizationCodeDialogFragmentArgs
import com.simrussia.app.view.category.CategoryFragmentArgs
import com.xwray.groupie.Group
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

class CategoryViewModel @AssistedInject constructor(
    @Assisted private val args: CategoryFragmentArgs,
    private val brandsUseCase: ContentHomeUseCase,
    private val contentCatalogUseCase: ContentCatalogUseCase,
    private val infoUserUseCase: InfoUserUseCase,
    private val userInteractor: UserInteractor,
) : BaseViewModel() {
    private val _dataItems: MutableLiveData<List<Group>> =
        MutableLiveData(emptyList())
    val dataItems: LiveData<List<Group>> get() = _dataItems

    fun init() {
        launchMain {
            _dataItems.postValue(generationContent())
        }
    }


    private fun generationContent(): List<Group> {
        val items = ArrayList<Group>()
        items += VerticalSpaceItem(50)
        items += ImageCategoryItem(args.category)
        items += VerticalSpaceItem(32)

        args.category.includeSections.forEach {
            items += SectionItem(it.title)
        }
        items += VerticalSpaceItem(14)
        items += GrayButtonItem("СМОТРЕТЬ ВСЕ")
        items += VerticalSpaceItem(130)
        return items
    }


    companion object {
        @Suppress("UNCHECKED_CAST")
        fun provideFactory(
            assistedFactory: AssistedFactory,
            args: CategoryFragmentArgs
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create(args) as T
            }
        }
    }

    @dagger.assisted.AssistedFactory
    interface AssistedFactory {
        fun create(
            args: CategoryFragmentArgs
        ): CategoryViewModel
    }
}