package com.simrussia.app.viewmodel.brands.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemLetterBinding
import com.simrussia.app.databinding.ItemNoSearchBinding

class NoSearchItem(
    private val letter: String?
) : BaseBindingItem<ItemNoSearchBinding>(R.layout.item_no_search) {
    override fun bind(viewBinding: ItemNoSearchBinding, position: Int) {
        viewBinding.letterText.text = letter
    }

    override fun initializeViewBinding(view: View) =
        ItemNoSearchBinding.bind(view)
}