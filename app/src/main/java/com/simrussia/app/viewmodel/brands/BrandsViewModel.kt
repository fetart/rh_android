package com.simrussia.app.viewmodel.brands

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.simrussa.models.home.CategoryDom
import com.simrussia.app.base.BaseViewModel
import com.simrussia.app.interactor.UserInteractor
import com.simrussia.app.items.*
import com.simrussia.app.usecase.ContentCatalogUseCase
import com.simrussia.app.usecase.ContentHomeUseCase
import com.simrussia.app.usecase.InfoUserUseCase
import com.simrussia.app.viewmodel.brands.items.BrandListItem
import com.simrussia.app.viewmodel.brands.items.LetterItem
import com.simrussia.app.viewmodel.brands.items.NoSearchItem
import com.xwray.groupie.Group
import dagger.assisted.AssistedInject
import java.util.*
import kotlin.collections.ArrayList

class BrandsViewModel @AssistedInject constructor(
    private val brandsUseCase: ContentHomeUseCase,
    private val contentCatalogUseCase: ContentCatalogUseCase,
    private val infoUserUseCase: InfoUserUseCase,
    private val userInteractor: UserInteractor,
) : BaseViewModel() {
    val listCategory = mutableListOf<CategoryDom>()
    private val _dataItems: MutableLiveData<List<Group>> =
        MutableLiveData(emptyList())
    val dataItems: LiveData<List<Group>> get() = _dataItems
    private val digitsRegex = Regex("[0-9]")
    private var query: String? = null

    fun init() {
        getBrandsAsync()
    }


    private fun generationContent(): List<Group> {
        val items = ArrayList<Group>()
        items += VerticalSpaceItem(50)

        items += VerticalSpaceItem(14)
        items += GrayButtonItem("СМОТРЕТЬ ВСЕ")
        items += VerticalSpaceItem(130)
        return items
    }

    private fun getBrandsAsync() {
        launchInVMScope {
            showLoading()
            brandsUseCase().collectSuccess {
                listCategory.clear()
                listCategory.addAll(it)
                _dataItems.postValue(createBrandItems(it))
            }
        }
    }

    fun queryChange(q: String) {
        query = q
        launchMain {
            _dataItems.postValue(createBrandItems(listCategory))
        }
    }

    private fun createBrandItems(brands: List<CategoryDom>): List<Group> {
        val items = ArrayList<Group>()
        var first = true
        items += VerticalSpaceItem(32)
        if (query.isNullOrEmpty()) {
            val groupedBrands = brands
                .groupBy { brand ->
                    val name = brand.title
                    if (name.isNullOrEmpty()) {
                        ""
                    } else {
                        val firstLetter =
                            name.first().toString().toUpperCase(Locale.getDefault())
                        if (firstLetter.matches(digitsRegex)) {
                            "0-9"
                        } else {
                            firstLetter
                        }
                    }
                }.toSortedMap()
            groupedBrands.forEach { entry ->
                items += LetterItem(entry.key)
                items += entry.value.map { BrandListItem(it) }
                items += VerticalSpaceItem(8)
            }
        } else {
            items += brands.filter { it.title.contains(query!!, true) }.sortedBy { it.title }
                .map {
                    if (first) {
                        first = false
                        BrandListItem(it, query?.length ?: 0)
                    } else BrandListItem(it)
                }
        }
        if (items.size <= 1) {
            items += NoSearchItem("Такой бренд не найден")
        }
        return items
    }


    companion object {
        @Suppress("UNCHECKED_CAST")
        fun provideFactory(
            assistedFactory: AssistedFactory
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        }
    }

    @dagger.assisted.AssistedFactory
    interface AssistedFactory {
        fun create(
        ): BrandsViewModel
    }
}