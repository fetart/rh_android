package com.simrussia.app.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemInfoUserBinding
import com.simrussa.models.home.HeadDom
import com.simrussia.app.databinding.ItemGrayButtonBinding
import com.simrussia.app.databinding.ItemSectionBinding
import com.simrussia.app.extensions.gone

class SectionItem(private val title: String) :
    BaseBindingItem<ItemSectionBinding>(R.layout.item_section) {
    override fun bind(viewBinding: ItemSectionBinding, position: Int) {
        viewBinding.root.text = title
    }

    override fun initializeViewBinding(view: View): ItemSectionBinding {
        return ItemSectionBinding.bind(view).apply {
        }
    }
}