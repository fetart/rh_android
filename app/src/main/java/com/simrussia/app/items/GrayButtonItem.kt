package com.simrussia.app.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemInfoUserBinding
import com.simrussa.models.home.HeadDom
import com.simrussia.app.databinding.ItemGrayButtonBinding
import com.simrussia.app.extensions.gone

class GrayButtonItem(private val title: String) :
    BaseBindingItem<ItemGrayButtonBinding>(R.layout.item_gray_button) {
    override fun bind(viewBinding: ItemGrayButtonBinding, position: Int) {
        viewBinding.name.text = title
    }

    override fun initializeViewBinding(view: View): ItemGrayButtonBinding {
        return ItemGrayButtonBinding.bind(view).apply {
        }
    }
}