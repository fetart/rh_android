package com.simrussia.app.items

import android.view.View
import coil.load
import com.simrussa.models.home.CategoryDom
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemInfoUserBinding
import com.simrussa.models.home.HeadDom
import com.simrussia.app.databinding.ItemGrayButtonBinding
import com.simrussia.app.databinding.ItemImageCategoryBinding
import com.simrussia.app.databinding.ItemSectionBinding
import com.simrussia.app.extensions.gone

class ImageCategoryItem(
    private val categoryDom: CategoryDom,
) :
    BaseBindingItem<ItemImageCategoryBinding>(R.layout.item_image_category) {
    override fun bind(viewBinding: ItemImageCategoryBinding, position: Int) {
    }

    override fun initializeViewBinding(view: View): ItemImageCategoryBinding {
        return ItemImageCategoryBinding.bind(view).apply {
            if (categoryDom.link.isNotEmpty())
                root.load(categoryDom.link)
            if (categoryDom.src != 0)
                root.setImageResource(categoryDom.src)
        }
    }
}