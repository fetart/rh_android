package com.simrussia.app.items

import android.view.View
import androidx.core.view.updateLayoutParams
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemVerticalSpaceBinding
import com.simrussia.app.util.SizeUtils
import com.xwray.groupie.viewbinding.GroupieViewHolder
import com.xwray.groupie.Item
import java.util.*

class VerticalSpaceItem(
    private val sizeInDp: Int
) : BaseBindingItem<ItemVerticalSpaceBinding>(R.layout.item_vertical_space) {

    val idItem = random.nextInt()

    override fun isSameAs(other: Item<*>): Boolean {
        return false
    }

    override fun hasSameContentAs(other: Item<*>): Boolean {
        return false
    }

    override fun getId(): Long {
        return idItem.toLong()
    }

    override fun getViewType(): Int {
        return idItem.toInt()
    }

    override fun getPosition(item: Item<*>): Int {
        return idItem.toInt()
    }

    override fun createViewHolder(itemView: View): GroupieViewHolder<ItemVerticalSpaceBinding> {
        itemView.updateLayoutParams {
            height = SizeUtils.dp2px(sizeInDp)
        }
        return super.createViewHolder(itemView)
    }

    override fun bind(viewBinding: ItemVerticalSpaceBinding, position: Int) {}

    override fun initializeViewBinding(view: View): ItemVerticalSpaceBinding =
        ItemVerticalSpaceBinding.bind(view).apply {
        }

    companion object {
        val random = Random()
    }
}