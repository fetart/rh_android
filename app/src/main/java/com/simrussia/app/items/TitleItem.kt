package com.simrussia.app.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemTitleBinding

class TitleItem(
    private val title: String,
    private val onClick: (() -> Unit)? = null,
) : BaseBindingItem<ItemTitleBinding>(R.layout.item_title) {

    override fun getId(): Long {
        return title.hashCode().toLong()
    }

    override fun getViewType(): Int {
        return title.hashCode()
    }

    override fun bind(viewBinding: ItemTitleBinding, position: Int) {
        viewBinding.titleItem.text = title
    }

    override fun initializeViewBinding(view: View): ItemTitleBinding {
        return ItemTitleBinding.bind(view)
    }
}