package com.simrussia.app.items

import android.view.View
import coil.load
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemCategoryBinding
import com.simrussa.models.home.CategoryDom

class CategoryItem(
    private val categoryDom: CategoryDom,
    private val onClick: ((CategoryDom) -> Unit)? = null
) :
    BaseBindingItem<ItemCategoryBinding>(R.layout.item_category) {
    override fun getId(): Long {
        return categoryDom.id.toLong()
    }

    override fun getViewType(): Int {
        return categoryDom.id.toInt()
    }

    override fun bind(viewBinding: ItemCategoryBinding, position: Int) {
        if (categoryDom.title.isEmpty()) {
            viewBinding.root.visibility = View.INVISIBLE
            return
        }
        viewBinding.title.text = categoryDom.title
        if (categoryDom.link.isNotEmpty())
            viewBinding.image.load(categoryDom.link)
        if (categoryDom.src != 0)
            viewBinding.image.setImageResource(categoryDom.src)
    }

    override fun getSpanSize(spanCount: Int, position: Int): Int = 1
    override fun initializeViewBinding(view: View): ItemCategoryBinding {
        return ItemCategoryBinding.bind(view).apply {
        root.setOnClickListener {
            onClick?.invoke(categoryDom)
        }

        }
    }
}