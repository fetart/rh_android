package com.simrussia.app.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemRedButtonBinding

class RedButtonItem(private val title: String) :
    BaseBindingItem<ItemRedButtonBinding>(R.layout.item_red_button) {
    override fun bind(viewBinding: ItemRedButtonBinding, position: Int) {
        viewBinding.name.text = title
    }

    override fun initializeViewBinding(view: View): ItemRedButtonBinding {
        return ItemRedButtonBinding.bind(view).apply {
        }
    }
}