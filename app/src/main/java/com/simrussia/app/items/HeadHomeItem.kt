package com.simrussia.app.items

import android.view.View
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemInfoUserBinding
import com.simrussa.models.home.HeadDom
import com.simrussia.app.extensions.gone

class HeadHomeItem(private val head: HeadDom) :
    BaseBindingItem<ItemInfoUserBinding>(R.layout.item_info_user) {
    override fun bind(viewBinding: ItemInfoUserBinding, position: Int) {
        if (head.bonus.isEmpty() || head.bonus == "0")
            viewBinding.bonus.gone()
        viewBinding.bonus.text = head.bonus
        viewBinding.name.text = head.name
        viewBinding.status.text = head.status
    }

    override fun initializeViewBinding(view: View): ItemInfoUserBinding {
        return ItemInfoUserBinding.bind(view).apply {
        }
    }
}