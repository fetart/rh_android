package com.simrussia.app.di.components

import dagger.Component
import com.simrussia.app.di.modules.AppModule
import com.simrussia.app.di.modules.LocationModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        LocationModule::class
    ]
)
interface ApplicationComponent