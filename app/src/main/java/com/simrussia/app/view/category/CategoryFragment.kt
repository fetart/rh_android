package com.simrussia.app.view.category

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentCategoryBinding
import com.simrussia.app.viewmodel.category.CategoryViewModel
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CategoryFragment : BaseFragment(R.layout.fragment_category) {
    private val args by navArgs<CategoryFragmentArgs>()

    private val binding: FragmentCategoryBinding by viewBinding(
        FragmentCategoryBinding::bind
    )

    private val itemsAdapter: GroupieAdapter by lazy {
        GroupieAdapter()
    }

    private fun collectData() {
        viewModel.dataItems.observe(viewLifecycleOwner) {
            itemsAdapter.replaceAll(it)
        }
    }

    @Inject
    lateinit var viewModelFactory: CategoryViewModel.AssistedFactory

    private val viewModel: CategoryViewModel by viewModels {
        CategoryViewModel.provideFactory(viewModelFactory, args)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // collectDefaultEvents(viewModel)
        binding.back.setOnClickListener {
            findNavController().popBackStack()
        }
        with(binding.homeRv) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = itemsAdapter
        }
        collectData()
        viewModel.init()
        binding.titleBar.text = args.category.title
    }
}