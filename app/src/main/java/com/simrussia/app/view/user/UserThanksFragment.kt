package com.simrussia.app.view.user

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussa.models.user.TypeUser
import com.simrussa.models.web.WebViewParam
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentUserThanksBinding
import com.simrussia.app.extensions.gone
import com.simrussia.app.view.web_view.WebViewFragment

class UserThanksFragment : BaseFragment(R.layout.fragment_user_thanks) {
    private val args by navArgs<UserThanksFragmentArgs>()
    private val binding: FragmentUserThanksBinding by viewBinding(FragmentUserThanksBinding::bind)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (args.type) {
            TypeUser.PROF_MESSAGE -> {
                binding.title.setText(R.string.we_write)
            }
            TypeUser.MO_PROF_MESSAGE -> {
                binding.title.setText(R.string.we_write)
                binding.footer.gone()
            }
            TypeUser.NO_PROF_CALL -> {
                binding.footer.gone()
            }
        }
        binding.whatApp.setOnClickListener {
            sendMessageToWhatApp()
        }
        binding.btContinue.setOnClickListener {
            openWebSite()
            //openCatalog()
        }
    }

    private fun openWebSite() {
        UserThanksFragmentDirections.actionUserThanksFragmentToWebViewFragment(
            WebViewParam(WebViewFragment.URL + WebViewFragment.CATALOG)
        ).let {
            findNavController().navigate(it)
        }
    }

    private fun sendMessageToWhatApp() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "https://api.whatsapp.com/send?phone=+74959816584&text=Тестовое задание андроид"
                    )
                )
            )
        } catch (e: PackageManager.NameNotFoundException) {
            print(e.message)
        }
    }

    private fun openCatalog() {
        UserThanksFragmentDirections.actionUserThanksFragmentToHomeFragment().let {
            findNavController().navigate(it)
        }
    }
}