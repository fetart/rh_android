package com.simrussia.app.view.greetings.items

import android.view.View
import com.simrussa.models.greetings.GreetingsPageDom
import com.simrussia.app.R
import com.simrussia.app.base.BaseBindingItem
import com.simrussia.app.databinding.ItemGreetingsPageBinding

class GreetingsPageItem(
    private val greetingsPage: GreetingsPageDom,
    private val onClick: (() -> Unit)? = null,
) : BaseBindingItem<ItemGreetingsPageBinding>(R.layout.item_greetings_page) {


    override fun bind(viewBinding: ItemGreetingsPageBinding, position: Int) {
        viewBinding.apply {
            image.setImageResource(greetingsPage.image)
            title.text = greetingsPage.title
            description.text = greetingsPage.description
        }
    }

    override fun initializeViewBinding(view: View): ItemGreetingsPageBinding {
        return ItemGreetingsPageBinding.bind(view)
    }
}