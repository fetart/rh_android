package com.simrussia.app.view.user

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussa.models.user.TypeUser
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentUserWelcomeBinding

class UserWelcomeFragment : BaseFragment(R.layout.fragment_user_welcome) {
    private val binding by viewBinding(FragmentUserWelcomeBinding::bind)
    private val args by navArgs<UserWelcomeFragmentArgs>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (args.type) {
            TypeUser.NO_ANON -> {
                binding.description.setText(R.string.have_already_bought)
                binding.otherNumber.setText(R.string.late_next_catalog)
            }
        }
        binding.btContinue.setOnClickListener {
            if (args.type == TypeUser.NO_ANON)
                openThanksScreen(TypeUser.NO_PROF_CALL)
            else
                openThanksScreen(TypeUser.PROF_CALL)
        }
        binding.otherNumber.setOnClickListener {
            if (binding.otherNumber.text == getString(R.string.late_next_catalog)) {
                UserWelcomeFragmentDirections.actionUserWelcomeFragmentToHomeFragment().let {
                    findNavController().navigate(it)
                }
            } else
                UserWelcomeFragmentDirections.actionUserWelcomeFragmentToUserNumber().let {
                    findNavController().navigate(it)
                }
        }

        binding.whatApp.setOnClickListener {
            sendMessageToWhatApp()
        }
        binding.writeMessage.setOnClickListener {
            if (args.type == TypeUser.NO_ANON)
                openThanksScreen(TypeUser.MO_PROF_MESSAGE)
            else
                openThanksScreen(TypeUser.PROF_MESSAGE)
        }
    }

    private fun openThanksScreen(args: TypeUser) {
        UserWelcomeFragmentDirections.actionUserWelcomeFragmentToUserThanksFragment(args).let {
            findNavController().navigate(it)
        }
    }
    private fun sendMessageToWhatApp() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "https://api.whatsapp.com/send?phone=+74959816584&text=Тестовое задание андроид"
                    )
                )
            )
        } catch (e: PackageManager.NameNotFoundException) {
            print(e.message)
        }
    }
}