package com.simrussia.app.view.bramds

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentBrandsBinding
import com.simrussia.app.viewmodel.brands.BrandsViewModel
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BrandsFragment : BaseFragment(R.layout.fragment_brands) {
    private val binding by viewBinding(FragmentBrandsBinding::bind)

    @Inject
    lateinit var viewModelFactory: BrandsViewModel.AssistedFactory
    private val itemsAdapter: GroupieAdapter by lazy {
        GroupieAdapter()
    }
    private val viewModel: BrandsViewModel by viewModels {
        BrandsViewModel.provideFactory(viewModelFactory)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectDefaultEvents(viewModel)
        setup()
        collectData()
        viewModel.init()
    }

    private fun setup() {
        binding.clearText.setOnClickListener {
            binding.search.setText("")
        }
        binding.search.addTextChangedListener {
            if (it.toString().isNotEmpty()) {
                binding.clearText.setImageResource(R.drawable.ic_cross)
            } else {
                binding.clearText.setImageResource(R.drawable.ic_search)
            }
            viewModel.queryChange(it.toString())
        }
        with(binding.homeRv) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = itemsAdapter
        }
    }

    private fun collectData() {
        viewModel.dataItems.observe(viewLifecycleOwner) {
            itemsAdapter.replaceAll(it)
        }
    }
}