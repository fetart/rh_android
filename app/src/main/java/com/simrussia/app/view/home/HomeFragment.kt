package com.simrussia.app.view.home

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentHomeBinding
import com.simrussia.app.extensions.launchInMain
import com.simrussia.app.util.GridHorizontalSpaceItemDecoration
import com.simrussia.app.viewmodel.home.ShowcaseViewModel
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import javax.inject.Inject
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.simrussia.app.extensions.gone
import com.simrussia.app.extensions.visible
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class HomeFragment : BaseFragment(R.layout.fragment_home) {
    private val binding by viewBinding(FragmentHomeBinding::bind)
    private val _bottomBarVisibility = MutableStateFlow(true)


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }



    private fun setupView() {

    }

    private fun showBottomBar() {
        binding.bottomBarInclude.root.animate()
            .setDuration(500L)
            .y(binding.bottomBarInclude.root.top.toFloat())
            .start()
    }

    private fun hideBottomBar() {
        binding.bottomBarInclude.root.animate()
            .setDuration(500L)
            .translationYBy(binding.root.height.toFloat())
            .start()
    }
}