package com.simrussia.app.view.showcase

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentHomeBinding
import com.simrussia.app.databinding.FragmentShowcaseBinding
import com.simrussia.app.extensions.gone
import com.simrussia.app.extensions.launchInMain
import com.simrussia.app.extensions.visible
import com.simrussia.app.util.GridHorizontalSpaceItemDecoration
import com.simrussia.app.view.home.HomeFragmentDirections
import com.simrussia.app.viewmodel.home.ShowcaseViewModel
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import javax.inject.Inject

@AndroidEntryPoint
class ShowcaseFragment : BaseFragment(R.layout.fragment_showcase) {
    private val binding by viewBinding(FragmentShowcaseBinding::bind)
    private val _bottomBarVisibility = MutableStateFlow(true)
    private val _statusBarVisibility = MutableStateFlow(false)
    private var positionTopPanelFilters: Int = -1
        get() {
            return if (field < 0) {
                field = getPositionPanel()
                return field
            } else
                field
        }

    private fun getPositionPanel(): Int {
        with(binding.homeRv) {
            return getChildLayoutPosition(findViewById<ViewGroup>(R.id.title_item)).apply {
            }
        }
    }


    private val itemsAdapter: GroupieAdapter by lazy {
        GroupieAdapter().apply {
            spanCount = 3
        }
    }

    @Inject
    lateinit var viewModelFactory: ShowcaseViewModel.AssistedFactory

    private val showcaseViewModel: ShowcaseViewModel by viewModels {
        ShowcaseViewModel.provideFactory(viewModelFactory)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectDefaultEvents(showcaseViewModel)
        setupView()
        collectProfileData()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showcaseViewModel.init()
    }

    private fun collectProfileData() {
        showcaseViewModel.profileItems.observe(viewLifecycleOwner) {
            itemsAdapter.replaceAll(it)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setupView() {
        with(binding) {
            launchInMain {
                _bottomBarVisibility.debounce(50).collect {
                    if (it) {
                        showBottomBar()
                    } else {
                        hideBottomBar()
                    }
                }
            }
            launchInMain {
                _statusBarVisibility.collect {
                    if (it) {
                        hideStatusBar()
                    } else {
                        showStatusBar()
                    }
                }
            }
            bottomBarInclude.itemProfile.setOnClickListener {
                openProfile()
            }

            bottomBarInclude.itemBrand.setOnClickListener {
                openBrands()
            }

            with(homeRv) {
                layoutManager = GridLayoutManager(requireContext(), 3).apply {
                    spanSizeLookup = itemsAdapter.spanSizeLookup
                }
                addItemDecoration(
                    GridHorizontalSpaceItemDecoration(requireContext())
                )
                adapter = itemsAdapter
                setupScrollListener()
            }
        }
    }

    private fun showBottomBar() {
        binding.bottomBarInclude.root.animate()
            .setDuration(500L)
            .y(binding.bottomBarInclude.root.top.toFloat())
            .start()
    }

    private fun hideBottomBar() {
        binding.bottomBarInclude.root.animate()
            .setDuration(500L)
            .translationYBy(binding.root.height.toFloat())
            .start()
    }

    private fun hideStatusBar() {
        binding.statusBar.animate()
            .setDuration(300L)
            .translationYBy(500f)
            .start()
    }

    private fun showStatusBar() {
        binding.statusBar.animate()
            .setDuration(500L)
            .y(0f)
            .start()
    }

    private fun RecyclerView.setupScrollListener() {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                updateUiControls(recyclerView, dy)
            }

            fun updateUiControls(recyclerView: RecyclerView, dy: Int) {
                val firstItem =
                    (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                if (dy > 0) {
                    launchInMain { _bottomBarVisibility.emit(false) }
                } else {
                    launchInMain { _bottomBarVisibility.emit(true) }
                }

                if (firstItem >= positionTopPanelFilters && firstItem > 0) {
                    launchInMain { _statusBarVisibility.emit(true) }
                    binding.titleItem.root.visible()
                    Log.e("setupScrollListener", "visible")

                } else {
                    launchInMain { _statusBarVisibility.emit(false) }
                    binding.titleItem.root.gone()
                    Log.e("setupScrollListener", "gone")
                }

            }
        })
    }

    private fun openBrands() {
        HomeFragmentDirections.actionHomeFragmentToBrandsFragment().let {
            findNavController().navigate(it)
        }
    }

    private fun openProfile() {
        //   findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
    }

}