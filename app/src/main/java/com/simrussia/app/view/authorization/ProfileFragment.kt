package com.simrussia.app.view.authorization

import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import androidx.fragment.app.viewModels
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

import android.content.Intent

import android.content.pm.PackageManager

import android.net.Uri
import android.view.KeyEvent
import androidx.core.view.marginBottom
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.core.widget.doOnTextChanged
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentProfileBinding
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.extensions.*
import com.simrussia.app.viewmodel.authorization.AuthorizationByNumberViewModel

@AndroidEntryPoint
class ProfileFragment : BaseFragment(R.layout.fragment_profile) {
    private val binding: FragmentProfileBinding by viewBinding(FragmentProfileBinding::bind)

    private val itemsAdapter = GroupieAdapter()
    private val affineFormats: MutableList<String> = ArrayList()

    @Inject
    lateinit var viewModelFactory: AuthorizationByNumberViewModel.AssistedFactory

    private val viewModel: AuthorizationByNumberViewModel by viewModels {
        AuthorizationByNumberViewModel.provideFactory(viewModelFactory)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectDefaultEvents(viewModel)

        setupView()
        collectProfileData()
    }

    private fun addAnimation() {
        val body = binding.body
        binding.btContinue.setOnClickListener {
            val number = binding.number.text.toString().deleteMaskByPhoneNumber()
            body.animate()
                .y((body.top).toFloat())
                .setDuration(300L)
                .withEndAction {
                    viewModel.sendNumber(number)
                }
                .withStartAction {
                    launchInMain {
                        with(binding.number) {
                            requestFocus()
                            hideKeyboard()
                        }
                        binding.title.updateMargin(
                            binding.title.marginLeft,
                            53.toPx(),
                            binding.title.marginRight,
                            binding.title.marginBottom
                        )
                    }
                }.start()
        }

        body.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val image = binding.imageLogo
                body.viewTreeObserver.removeOnGlobalLayoutListener(this)
                body.animate()
                    .setDuration(300)
                    .translationYBy((body.top + image.height) * -1f)
                    .setStartDelay(800L)
                    .withStartAction {
                        launchInMain {
                            with(binding.number) {
                                binding.title.updateMargin(
                                    binding.title.marginLeft,
                                    120.toPx(),
                                    binding.title.marginRight,
                                    binding.title.marginBottom
                                )
                                requestFocus()
                                showKeyboard()
                            }
                        }
                    }.start()
            }
        })
    }


    private fun collectProfileData() {
        viewModel.profileItems.observe(viewLifecycleOwner) {
            itemsAdapter.replaceAll(it)
        }
    }

    private fun setupView() {
        addAnimation()
        addMask()
        binding.sendWhatApp.setOnClickListener {
            sendMessageToWhatApp()
        }
        binding.cross.setOnClickListener {
            binding.number.setText("")
        }

        binding.number.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    if (binding.btContinue.isEnabled)
                        binding.btContinue.performClick()
                    return false
                }
                return false
            }
        })
    }


    private fun sendMessageToWhatApp() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "https://api.whatsapp.com/send?phone=+74959816584&text=Тестовое задание андроид"
                    )
                )
            )
        } catch (e: PackageManager.NameNotFoundException) {
            print(e.message)
        }
    }

    private fun addMask() = with(binding.number) {
        doOnTextChanged() { text, start, before, count ->
            text?.let {
                if (count > 10 && before == 0) {
                    var newText = it.substring(it.length - 11)
                    if ((newText.length == 10 || newText.length == 11)
                        && (newText.getOrNull(0) == '7' || newText.getOrNull(0) == '8')
                    ) {
                        newText = newText.substring(1)
                        setText(newText)
                    }
                }
            }
        }
        MaskedTextChangedListener.installOn(
            this,
            "+7 ([000]) [000]-[00]-[00]",
            affineFormats.apply { add("+7 ([000]) [000]-[00]-[00]") },
            AffinityCalculationStrategy.PREFIX,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(
                    maskFilled: Boolean,
                    extractedValue: String,
                    formattedValue: String
                ) {
                    if (formattedValue.length < 4) setText("+7")
                    buttonEnable(extractedValue.count() == 10)
                    if (extractedValue.count() >= 1) {
                        binding.cross.visible()
                    } else {
                        binding.cross.gone()
                    }
                    //  binding.btContinue.isEnabled = extractedValue.count() == 10
                }
            }
        ).apply {
            //hint = this.placeholder()
        }
    }

    private fun buttonEnable(b: Boolean) {
        binding.btContinue.isEnabled = b
        if (!b) {
            binding.titleBt.setTextColor(resources.getColor(R.color.bt_disable))
            binding.btContinue.background.setTint(resources.getColor(R.color.gray_category))
        } else {
            binding.titleBt.setTextColor(resources.getColor(R.color.white))
            binding.btContinue.background.setTint(resources.getColor(R.color.base_red_color))
        }
    }

}