package com.simrussia.app.view.web_view

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.webkit.WebView
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentWebViewBinding
import java.math.BigInteger
import java.security.MessageDigest

class WebViewFragment : BaseFragment(R.layout.fragment_web_view) {

    private val args: WebViewFragmentArgs by navArgs()
    private val viewBinding: FragmentWebViewBinding by viewBinding(
        FragmentWebViewBinding::bind
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity()
            .onBackPressedDispatcher
            .addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    toBack()
                }
            }
            )

        with(viewBinding.webView) {
            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    url?.let {

                    }
                }
            }
            loadUrl(args.param.url)
            val webSettings: WebSettings = settings
            webSettings.javaScriptEnabled = true
        }
    }

    fun md5(input: String): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }

    private fun toBack() {
        findNavController().popBackStack()
    }

    companion object {
        const val URL = "http://redharemarket.ru/"
        const val CATALOG = "/catalog"
        const val brands = "/brands"
    }
}

