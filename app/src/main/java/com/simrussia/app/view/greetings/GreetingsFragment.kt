package com.simrussia.app.view.greetings

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.MarginPageTransformer
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussa.models.greetings.GreetingsPageDom
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.GreetingsFragmentBinding
import com.simrussia.app.databinding.SplashFragmentBinding
import com.simrussia.app.view.greetings.items.GreetingsPageItem
import com.xwray.groupie.GroupieAdapter

class GreetingsFragment : BaseFragment(R.layout.greetings_fragment) {
    private val binding: GreetingsFragmentBinding by viewBinding(GreetingsFragmentBinding::bind)

    private val greetingsAdapter by lazy { GroupieAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        with(binding) {
            btContinue.setOnClickListener {
//                if (vpGreetings.currentItem <= vpGreetings.childCount)
//                    vpGreetings.setCurrentItem(vpGreetings.currentItem + 1, true)
//                else
                    openAuthorization()

            }
            greetingsAdapter.replaceAll(generationItem())
            vpGreetings.adapter = greetingsAdapter
            vpGreetings.setPageTransformer(MarginPageTransformer(32))
            dotsIndicator.setViewPager2(vpGreetings)
        }
    }

    private fun openAuthorization() {
        GreetingsFragmentDirections.actionGreetingsFragmentToProfileFragment().let {
            findNavController().navigate(it)
        }
    }

    private fun generationItem() = listOf(
        GreetingsPageItem(
            GreetingsPageDom(
                1,
                getString(R.string.red_hare),
                getString(R.string.unique_marketplace),
                R.drawable.ic_barbering
            )
        ),
        GreetingsPageItem(
            GreetingsPageDom(
                2,
                getString(R.string.convenient_catalog),
                getString(R.string.fast_navigation),
                R.drawable.ic_barbering2
            )
        ),
        GreetingsPageItem(
            GreetingsPageDom(
                1,
                getString(R.string.convenient_catalog),
                getString(R.string.confirmation_status),
                R.drawable.ic_barbering3
            )
        ),
    )

}