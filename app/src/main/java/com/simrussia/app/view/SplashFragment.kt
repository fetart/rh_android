package com.simrussia.app.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.SplashFragmentBinding
import com.simrussia.app.extensions.launchInMain
import com.simrussia.app.interactor.UserInteractor
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : BaseFragment(R.layout.splash_fragment) {

    @Inject
    lateinit var userInteractor: UserInteractor

    private val binding: SplashFragmentBinding by viewBinding(SplashFragmentBinding::bind)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler(Looper.getMainLooper()).postDelayed({
            openAuthorization()
        }, 2400)

    }

    private fun openAuthorization() {
        launchInMain {
            val g = userInteractor.getUserInfo()
            if (userInteractor.isAuthUser())
                SplashFragmentDirections.actionSplashFragmentToHomeFragment().let {
                    findNavController().navigate(it)
                }
            else
                SplashFragmentDirections.actionSplashFragmentToGreetingsFragment().let {
                    findNavController().navigate(it)
                }
        }
    }

}