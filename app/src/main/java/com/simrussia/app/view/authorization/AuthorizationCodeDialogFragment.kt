package com.simrussia.app.view.authorization

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.simrussia.app.R
import com.simrussia.app.base.BaseFragment
import com.simrussia.app.databinding.FragmentAuthorizationCodeBinding
import com.simrussia.app.extensions.hideKeyboard
import com.simrussia.app.extensions.launchInMain
import com.simrussia.app.extensions.showKeyboard
import com.simrussia.app.viewmodel.authorization.AuthorizationCodeViewModel
import com.simrussia.utils.Timer
import com.simrussia.utils.TimerSubscriber
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AuthorizationCodeDialogFragment : BaseFragment(R.layout.fragment_authorization_code),
    TimerSubscriber {
    private val binding: FragmentAuthorizationCodeBinding by viewBinding(
        FragmentAuthorizationCodeBinding::bind
    )

    @Inject
    lateinit var viewModelAssistedFactory: AuthorizationCodeViewModel.AssistedFactory
    private val args by navArgs<AuthorizationCodeDialogFragmentArgs>()

    private val viewModel: AuthorizationCodeViewModel by viewModels {
        AuthorizationCodeViewModel.provideFactory(viewModelAssistedFactory, args)
    }
    private val timer = Timer(1)

    override fun tick() {
        if (timer.getCounter() == 0) {
            timer.pause()
            with(binding.requestCode) {
                setText(R.string.request_code_again)
                setTextAppearance(R.style.Text_BodyBold)
                textSize = 12f
                setTextColor(getColor(R.color.base_red_color))
            }
            return
        }
        with(binding.requestCode) {
            textSize = 16f
            setTextAppearance(R.style.Text_BodyLabel)
            text = requireContext().getString(R.string.request_code, timer.getCounter().toString())
            setTextColor(getColor(R.color.black))
        }
    }

    override fun onResume() {
        super.onResume()
        timer.addSubscriber(this)
        // if (timer.isPause())
        //  timer.start()
    }

    override fun onPause() {
        super.onPause()
        timer.removeSubscriber()
        // timer.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
        timer.removeSubscriber()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timer.addSubscriber(this)
        collectDefaultEvents(viewModel)

        collectCodeState()
        messageCollect()
        collectAuthState()
        binding.back.setOnClickListener {
            hideKeyboard()
            findNavController().popBackStack()
        }
        binding.requestCode.setOnClickListener {
            if (timer.getCounter() == 0) {
                errorStateCode(false)
                timer.start()
                viewModel.sendCodeAgain()
            }
        }
        timer.start()

        with(binding.code) {
            setOnAllFilledListener {
                //viewModel.onAuthorization(it)
            }
            setOnTextChangedListener {
                if (it.count() == 4) viewModel.onAuthorization(it)
                binding.code.vcWrapperColor = (R.color.black)
                binding.code.vcTextColor = getColor(R.color.black)
            }
            requestFocus()
            postDelayed({
                showKeyboard()
            }, 300)
        }
    }

    private fun collectCodeState() {
        launchInMain {
            viewModel.stateError.observe(viewLifecycleOwner, { errorState ->
                errorStateCode(errorState)
            })
        }
    }

    private fun messageCollect() {

    }

    private fun collectAuthState() {
        launchInMain {
            viewModel.isAutho.observe(viewLifecycleOwner, { isAutho ->
                //  if (isAutho) setFragmentResult(AUTHO, Bundle())
            })
        }
    }

    @SuppressLint("ResourceAsColor")
    private fun errorStateCode(flag: Boolean) {
        if (flag) {
            binding.description.setText(R.string.oops_check_again)
            binding.code.vcWrapperColor = getColor(R.color.base_red_color)
            binding.code.vcTextColor = getColor(R.color.base_red_color)
            binding.code.post {
                //  binding.code.vcText = ""
            }
            binding.code.requestFocus()
            binding.code.postDelayed({
                binding.code.showKeyboard()
            }, 300)
        } else {
            binding.code.vcWrapperColor = (R.color.black)
            binding.code.vcTextColor = getColor(R.color.black)
        }
    }

    companion object {
        const val AUTHO = "AUTHO"
    }
}
