package com.simrussia.app.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SimrussiaApp : Application() {
}