package com.simrussia.core.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.simrussia.core.qualifiers.BaseUrlQualifier
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import com.simrussa.persistance.ApplicationPreferences
import com.simrussa.remote.api.IApiHomeService
import com.simrussa.remote.api.IApiService
import com.simrussa.remote.api.IAuthorizationApiService
import com.simrussa.remote.newtwork.ApiServiceFactory
import com.simrussa.remote.newtwork.interceptors.AddHeadersInterceptor
import com.simrussa.remote.newtwork.interceptors.AuthInterceptor
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideAuthInterceptor(
        prefs: ApplicationPreferences,
        @BaseUrlQualifier baseUrl: String,
        gson: Gson
    ): AuthInterceptor {
        return AuthInterceptor(prefs, gson, baseUrl)
    }

    @Provides
    @Singleton
    fun provideAddHeadersInterceptor(prefs: ApplicationPreferences): AddHeadersInterceptor {
        // val version = "(${BuildConfig.VERSION_NAME}) ${BuildConfig.VERSION_CODE}"
        return AddHeadersInterceptor(prefs, "version")
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        addHeadersInterceptor: AddHeadersInterceptor,
        authInterceptor: AuthInterceptor
    ): OkHttpClient {
        return ApiServiceFactory.createOkHttpClient(addHeadersInterceptor, authInterceptor)
    }

    @Provides
    @BaseUrlQualifier
    fun provideBaseApiUrl(): String {
        return "https://api.redharemarket.ru/"//BuildConfig.BASE_URL
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        @BaseUrlQualifier baseUrl: String,
        client: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return ApiServiceFactory.createRetrofit(baseUrl, client, gson)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return ApiServiceFactory.createGson()
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): IApiService {
        return retrofit.create(IApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideHomeApiService(apiService: IApiService): IApiHomeService {
        return apiService
    }

    @Provides
    @Singleton
    fun provideAuthorizationApiService(apiService: IApiService): IAuthorizationApiService {
        return apiService
    }
}