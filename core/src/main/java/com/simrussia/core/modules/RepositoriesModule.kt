package com.simrussia.core.modules

import com.simrussa.persistance.ApplicationPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.simrussa.remote.api.IAuthorizationApiService
import com.simrussia.repositories.authorization.AuthorizationRepositoryImpl
import com.simrussia.repositories.authorization.IAuthorizationRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoriesModule {

    @Provides
    @Singleton
    fun provideAuthorizationRepository(
        authorizationApi: IAuthorizationApiService,
        appPrefs: ApplicationPreferences
    ): IAuthorizationRepository {
        return AuthorizationRepositoryImpl(authorizationApi, appPrefs)
    }

}