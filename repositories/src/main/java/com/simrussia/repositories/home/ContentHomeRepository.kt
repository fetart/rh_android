package com.simrussia.repositories.home

import com.simrussa.remote.api.IApiHomeService
import javax.inject.Inject
import kotlin.random.Random

class ContentHomeRepository @Inject constructor(
    private val apiHomeService: IApiHomeService,
) {
    companion object {
        val random = Random(10000)
    }

    suspend fun getCatalog() = apiHomeService.getCatalog()

    suspend fun getBrands() = apiHomeService.getBrands()


    private fun generateAmazingContent(): String {
        return ""
//        return HomeDom(
//            head = HeadDom(
//                status = "Базовый статус",
//                name = "Константин Константинов",
//                bonus = "140"
//            ),
//            categories = listOf(
//                CategoryDom(
//                    random.nextInt(),
//                    "Ножницы", R.drawable.ic_1
//                ),
//                CategoryDom(random.nextInt(), "Расходные материалы", R.drawable.ic_2),
//                CategoryDom(random.nextInt(), "Окрашивание волос", R.drawable.ic_1),
//                CategoryDom(random.nextInt(), "Для барберов", R.drawable.ic_1),
//                CategoryDom(random.nextInt(), "Оборудование для обучения", R.drawable.ic_2),
//                CategoryDom(random.nextInt(), "Пеньюары и фартуки", R.drawable.ic_2),
//                CategoryDom(random.nextInt(), "Расчески и брашинги", R.drawable.ic_2),
//                CategoryDom(random.nextInt(), "Фены и стайлеры", R.drawable.ic_2),
//                CategoryDom(random.nextInt(), "Фены и стайлеры", R.drawable.ic_2),
//                CategoryDom(random.nextInt(), "", 0),
//                CategoryDom(random.nextInt(), "Дезинфекция и гигиена", R.drawable.ic_1),
//                CategoryDom(random.nextInt(), "", 0),
        //)
        // )
    }
}