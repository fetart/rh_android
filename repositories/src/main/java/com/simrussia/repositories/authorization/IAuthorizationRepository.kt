package com.simrussia.repositories.authorization

import com.simrussa.remote.BaseResponse
import com.simrussa.remote.models.auth.AuthorizationResponse
import com.simrussa.remote.models.user.UserResponse
import com.simrussia.result.Result

interface IAuthorizationRepository {
    suspend fun sendPhone(phone: String): Result<BaseResponse<Map<Any, Any>>>
    suspend fun authorization(
        phone: String,
        code: String
    ): Result<BaseResponse<Map<Any, Any>>>

    suspend fun deauthorization(): Result<BaseResponse<AuthorizationResponse>>
    suspend fun getInfoUser(phone: String): Result<BaseResponse<UserResponse>>
    suspend fun setAuthData(userInfo: String?)
    suspend fun isAuth(): Boolean
    suspend fun getUserInfo(): String?
}