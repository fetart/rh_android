package com.simrussia.repositories.authorization

import com.simrussa.persistance.ApplicationPreferences
import com.simrussia.result.*
import com.simrussa.remote.BaseResponse
import com.simrussa.remote.api.IAuthorizationApiService
import com.simrussa.remote.models.auth.AuthorizationResponse
import javax.inject.Inject

class AuthorizationRepositoryImpl @Inject constructor(
    private val authorizationApi: IAuthorizationApiService,
    private val appPreferences: ApplicationPreferences,
) : IAuthorizationRepository {
    override suspend fun sendPhone(phone: String) =
        authorizationApi.sendPhone(phone)

    override suspend fun isAuth(): Boolean {
        return appPreferences.userInfo != null
    }

    override suspend fun setAuthData(userInfo: String?) {
        appPreferences.userInfo = userInfo
    }

    override suspend fun getUserInfo(): String? {
        return appPreferences.userInfo
    }

    override suspend fun authorization(
        phone: String,
        code: String
    ): Result<BaseResponse<Map<Any, Any>>> {
        return authorizationApi.authorization(phone, code)
    }

    override suspend fun getInfoUser(phone: String) = authorizationApi.getInfoUser(phone)


    override suspend fun deauthorization(): Result<BaseResponse<AuthorizationResponse>> =
        authorizationApi.deauthorization()

}
