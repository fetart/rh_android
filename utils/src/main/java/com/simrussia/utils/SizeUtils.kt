package com.simrussia.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Point
import android.os.Build
import android.view.WindowManager
import androidx.annotation.Px
import kotlin.math.ceil

object SizeUtils {

    private var lastKnownWidth: Int = -1

    fun refreshScreenSize(context: Context) {
        screenWidth(context, refresh = true)
    }

    fun screenWidth(context: Context, refresh: Boolean = false): Int {
        if (refresh || lastKnownWidth == -1) {
            val point = Point()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                context.display?.getRealSize(point)
            } else {
                val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                windowManager.defaultDisplay.getRealSize(point)
            }
            lastKnownWidth = point.x
        }
        return lastKnownWidth
    }

    @Px
    fun dp2px(dpValue: Int): Int {
        val scale = Resources.getSystem().displayMetrics.density
        return (ceil(dpValue * scale)).toInt()
    }

    fun px2dp(@Px pxValue: Int): Int {
        val scale = Resources.getSystem().displayMetrics.density
        return (ceil(pxValue/ scale)).toInt()
    }

}