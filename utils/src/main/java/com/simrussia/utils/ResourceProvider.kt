package com.simrussia.utils

import android.content.Context
import android.content.res.Resources
import androidx.annotation.ColorRes
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResourceProvider @Inject constructor(@ApplicationContext context: Context) {

    private val resources: Resources = context.resources

    fun getString(@StringRes id: Int): String = resources.getString(id)
    fun getColor(@ColorRes id: Int): Int = resources.getColor(id)
    fun getString(@StringRes id: Int, vararg formatArgs: Any?): String =
        resources.getString(id, formatArgs)

    fun getQuantityString(@PluralsRes id: Int, quantity: Int, vararg args: Any?) =
        resources.getQuantityString(id, quantity, *args)

}