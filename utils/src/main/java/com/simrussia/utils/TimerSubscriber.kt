package com.simrussia.utils

interface TimerSubscriber {
    fun tick()
}