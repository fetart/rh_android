package com.ladygentleman.app.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

object KeyboardUtils {

    fun hideKeyboard(view: View) {
        view.imm().hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboard(view: View) {
        view.imm().showSoftInput(view, 0)
    }

    private fun View.imm(): InputMethodManager {
        return this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

}