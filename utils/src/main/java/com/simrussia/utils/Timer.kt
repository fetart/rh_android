package com.simrussia.utils

import kotlinx.coroutines.*

class Timer(private val second: Long) {
    private var pause = false
    private var startTimer = false
    private val scope = CoroutineScope(Dispatchers.Default + SupervisorJob())
    private var subscriber: TimerSubscriber? = null
    private var counter = SECOND_WAIT

    fun getCounter() = counter

    fun addSubscriber(subscriber: TimerSubscriber) {
        this.subscriber = subscriber
    }

    fun removeSubscriber() {
        subscriber = null
    }

    private fun startCoroutineTimer() = scope.launch {
        while (true) {
            mainThread()
            delay(second * 1000)
        }
    }

    private fun mainThread() = scope.launch(Dispatchers.Main) {
        subscriber?.tick()
        counter = if (counter == 0) counter else --counter
    }

    fun start() {
        startTimer = true
        counter = SECOND_WAIT
        startCoroutineTimer()
    }

    fun cancel() {
        pause = false
        startTimer = false
        scope.coroutineContext.cancelChildren()
    }

    fun pause() {
        if (startTimer) {
            pause = true
            scope.coroutineContext.cancelChildren()
        }
    }

    fun isPause() = startTimer and pause

    companion object {
        const val SECOND_WAIT = 60
    }

}